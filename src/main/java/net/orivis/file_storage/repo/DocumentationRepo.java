package net.orivis.file_storage.repo;

import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.stereotype.Repository;
import net.orivis.file_storage.model.DocumentationModel;

@Repository
public interface DocumentationRepo extends OrivisRepository<DocumentationModel> {
}
