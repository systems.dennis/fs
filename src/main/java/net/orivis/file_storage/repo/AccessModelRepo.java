package net.orivis.file_storage.repo;

import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.stereotype.Repository;
import net.orivis.file_storage.model.AccessModel;

@Repository
public interface AccessModelRepo extends OrivisRepository<AccessModel> {

}
