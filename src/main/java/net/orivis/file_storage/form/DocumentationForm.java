package net.orivis.file_storage.form;

import lombok.Data;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.utils.OrivisDate;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;
import org.springframework.data.domain.Sort;
import net.orivis.file_storage.service.DocumentationService;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.DEFAULT_TYPES;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.utils.bean_copier.DateToUTCConverter;

import java.util.List;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;
import static net.orivis.shared.pojo_view.UIAction.*;

@Data
@PojoListView (actions = {ACTION_NEW, ACTION_SETTINGS})
public class DocumentationForm  implements OrivisPojo {


    @PojoFormElement
    @PojoListViewField(searchable = true)
    private String title;


    @PojoFormElement (type = HIDDEN)
    private Long id;


    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = DocumentationService.class )
    @PojoFormElement(type = DEFAULT_TYPES.OBJECT_SEARCH, remote = @Remote(searchName = "docs", searchField = "title", searchType = DEFAULT_TYPES.OBJECT_SEARCH))
    @PojoListViewField(searchable = true, type = DEFAULT_TYPES.OBJECT_SEARCH , remote = @Remote(searchName = "docs", searchField = "title", searchType = DEFAULT_TYPES.OBJECT_SEARCH))
    private Long parent;

    @PojoFormElement(type = DEFAULT_TYPES.NUMBER, remote = @Remote(searchType = DEFAULT_TYPES.NUMBER))
    @PojoListViewField(searchable = true, type = DEFAULT_TYPES.NUMBER , remote = @Remote( searchType = DEFAULT_TYPES.NUMBER))

    private Integer order;

    @OrivisTranformer(transformWith = DateToUTCConverter.class)
    private OrivisDate lastUpdated;


    @PojoFormElement(type = DEFAULT_TYPES.TEXT_AREA)
    @PojoListViewField(searchable = true)
    private String text;

    @PojoFormElement(available = false)
    @PojoListViewField(visible = false, actions = {@UIAction( ACTION_EDIT), @UIAction(value = ACTION_DELETE, allowOnMultipleRows = true)})
    private Integer actions;

    @Override
    public String asValue() {
        return title;
    }

    @PojoFormElement (type = DEFAULT_TYPES.FILES, remote = @Remote (searchType = DEFAULT_TYPES.FILES))
    private List<String> attachments;

    @Override
    public KeyValue defaultSearchOrderField() {
        return new KeyValue("title", Sort.Direction.DESC);
    }
}

