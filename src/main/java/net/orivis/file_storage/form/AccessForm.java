package net.orivis.file_storage.form;

import lombok.Data;
import net.orivis.file_storage.fs.FileFetcherController;
import net.orivis.file_storage.model.AccessMode;
import net.orivis.file_storage.validator.UniqueAccessValidator;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.postgres.form.OrivisPojo;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {"download", "new", "settings"})
public class AccessForm implements OrivisPojo {
    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private Long model;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    @Validation(UniqueAccessValidator.class)
    private AccessMode mode;

    @PojoFormElement(remote = @Remote(fetcher = "files",  controller = FileFetcherController.class))
    @PojoListViewField(searchable = true, remote = @Remote(fetcher = "files", controller = FileFetcherController.class))
    private Long userFileId;

    @PojoFormElement(type = HIDDEN)
    private Long id;
}
