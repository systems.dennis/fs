package net.orivis.file_storage.form;

import lombok.Data;
import net.orivis.shared.postgres.form.OrivisPojo;

import java.util.Date;
import java.util.Map;

@Data
public class UserFileForm implements OrivisPojo {
    private String originalName;

    private Date created = new Date();

    private Boolean pub = Boolean.FALSE;

    private String name;

    private String downloadUrl;

    private Map<String, String> sizes;
    private Boolean newlyCreated;

    private String type;

    private Long size;

    private Long id;

    private Boolean hidden;

}
