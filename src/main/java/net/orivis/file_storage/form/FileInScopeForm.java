package net.orivis.file_storage.form;

import lombok.Data;
import net.orivis.file_storage.service.FileServicePublic;
import net.orivis.file_storage.validator.UniqueFileScopeValidator;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.DEFAULT_TYPES;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.scopes.service.ScopeService;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {"download", "new", "settings"})
public class FileInScopeForm implements OrivisPojo {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = FileServicePublic.class)
    @PojoFormElement(required = true, type = DEFAULT_TYPES.OBJECT_SEARCH,
            remote = @Remote(fetcher = "files/public", searchType = DEFAULT_TYPES.OBJECT_SEARCH, searchField = "name", searchName = "public"))
    @PojoListViewField(type = DEFAULT_TYPES.OBJECT_SEARCH, searchable = true,
            remote = @Remote(fetcher = "files/public", searchType = DEFAULT_TYPES.OBJECT_SEARCH, searchField = "name", searchName = "public"))
    @Validation(UniqueFileScopeValidator.class)
    private Long userFile;

    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = ScopeService.class)
    @PojoFormElement(required = true, type = DEFAULT_TYPES.OBJECT_SEARCH,
            remote = @Remote(fetcher = "files/scope", searchType = DEFAULT_TYPES.OBJECT_SEARCH, searchField = "name", searchName = "scope"))
    @PojoListViewField(type = DEFAULT_TYPES.OBJECT_SEARCH, searchable = true,
            remote = @Remote(fetcher = "files/scope", searchType = DEFAULT_TYPES.OBJECT_SEARCH, searchField = "name", searchName = "scope"))
    private Long scope;

    @PojoListViewField(actions = {@UIAction(component = "edit"), @UIAction(component = "delete", allowOnMultipleRows = true)}, visible = false)
    @PojoFormElement(available = false)
    private Integer action;
}
