package net.orivis.file_storage.controller;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.service.FileServicePrivate;
import net.orivis.file_storage.service.FileServicePublic;
import net.orivis.shared.annotations.security.Secured;

@RestController()
@RequestMapping("/api/v2/files/cabinet")
@Secured
@CrossOrigin
public class MyResources {


    public final FileServicePrivate service;
    private FileServicePublic fileServicePublic;

    public MyResources(FileServicePrivate service, FileServicePublic fileServicePublic) {
        this.service = service;
        this.fileServicePublic = fileServicePublic;
    }

    @GetMapping(value = "/search/{scopeName}", produces = "application/json")
    @ResponseBody
    public ResponseEntity<Page<UserFile>> getFiles(
            @RequestParam(value = "page" , required = false)  Integer page,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "s", required = false) String query,
            @RequestParam(value = "onlyMy", required = false) Boolean onlyMy,
            @RequestParam(value = "onPublic", required = false) Boolean onPublic,
            @RequestParam(value = "onlyImages", required = false) Boolean onlyImages,
            @PathVariable String scopeName) {

        //security fix: always search only though my uploads
        onlyMy = true;
        if (onPublic == null || !onPublic){
            return ResponseEntity.ok( service.searchFiles(page, limit, query, onlyImages, onlyMy, scopeName));
        } else {
            return ResponseEntity.ok(fileServicePublic.searchFiles(page, limit, query, onlyImages, onlyMy, scopeName));
        }


    }

}
