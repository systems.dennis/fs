package net.orivis.file_storage.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.file_storage.service.FileMigrationService;

@RestController
@RequestMapping("/api/v2/migration")
public class FileMigrationController {

    private final FileMigrationService fileMigrationService;

    public FileMigrationController(FileMigrationService fileMigrationService) {
        this.fileMigrationService = fileMigrationService;
    }

    @PostMapping("/migrate")
    public ResponseEntity<String> migrateFiles() {
        try {
            fileMigrationService.migrate();
            return ResponseEntity.ok("Migration completed successfully.");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Migration failed: " + e.getMessage());
        }
    }

}

