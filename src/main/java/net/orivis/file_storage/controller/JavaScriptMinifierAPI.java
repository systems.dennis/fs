package net.orivis.file_storage.controller;

import net.logicsquad.minifier.MinificationException;
import net.logicsquad.minifier.Minifier;
import net.logicsquad.minifier.js.JSMinifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import net.orivis.file_storage.exceptions.FileException;


import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v2/minifier")
@CrossOrigin
public class JavaScriptMinifierAPI {

    @PostMapping("/minify")
    public Map<String, String> treatmentJavaScriptFiles(@RequestBody MinifyRequest minifyRequest) {
        try {

            if(minifyRequest == null) {
                    throw new FileException("Incorrect request body!", HttpStatus.BAD_REQUEST);
            }

            var separator = getSeparator();

            Map<String, String> jsFiles = filterJavaScriptFiles(scanFiles(minifyRequest.getDirectory()));

            createDestinationDirectory(minifyRequest);

            String[] partsOfOriginalDirectory = minifyRequest.getDirectory().split(separator);
            String originalFolderName = partsOfOriginalDirectory[partsOfOriginalDirectory.length - 1];

            for(Map.Entry<String, String> entry : jsFiles.entrySet()) {

                Path filePath = Paths.get(entry.getKey());
                String pathToFile = filePath.getParent().toString();

                String[] parts = pathToFile.split(originalFolderName);

                String newPathToFile;
                if(parts.length > 1) {
                    newPathToFile = parts[0] + minifyRequest.getDestinationFolderName() + parts[1];
                } else {
                    newPathToFile = parts[0] + minifyRequest.getDestinationFolderName();
                }
                new File(newPathToFile).mkdirs();

                String minifyFileName = removeFileExtension(entry.getValue()) + minifyRequest.getDestinationFileAddon() + ".1.js";
                Reader input = new FileReader(Paths.get(pathToFile, entry.getValue()).toString());
                Writer output = new FileWriter( newPathToFile + separator + minifyFileName);
                Minifier min = new JSMinifier(input);
                try {
                    min.minify(output);
                } catch (MinificationException e) {
                    e.printStackTrace();
                }

            }

            return jsFiles;

        } catch(Exception e) {
            e.printStackTrace();
        }

        return new HashMap<>();
    }

    private static void createDestinationDirectory(MinifyRequest minifyRequest) {
        String separator = getSeparator();

        File directory = new File(minifyRequest.getDirectory());
        String parentDirectory = directory.getParent();
        new File(parentDirectory + separator + minifyRequest.getDestinationFolderName()).mkdirs();

    }

    private static String getSeparator() {
        var separator = FileSystems.getDefault().getSeparator();
        if (separator.equals("\\")) {
            separator = "\\\\";
        }
        return separator;
    }

    private Map<String, String> filterJavaScriptFiles(Map<String, String> fileMap) {
        Map<String, String> jsFileMap = new HashMap<>();

        for (Map.Entry<String, String> entry : fileMap.entrySet()) {
            String fileName = entry.getValue();
            if (isJavaScriptFile(fileName)) {
                jsFileMap.put(entry.getKey(), fileName);
            }
        }

        return jsFileMap;
    }

    private boolean isJavaScriptFile(String fileName) {
        return fileName.toLowerCase().endsWith(".1.js");
    }

    private Map<String, String> scanFiles(String directoryPath) {
        Map<String, String> fileMap = new HashMap<>();
        File directory = new File(directoryPath);

        if (!directory.exists() || !directory.isDirectory()) {
            throw new FileException("Incorrect path to directory!", HttpStatus.BAD_REQUEST);
        }

        scanFilesRecursively(directory, fileMap);

        return fileMap;
    }


    private void scanFilesRecursively(File directory, Map<String, String> fileMap) {
        File[] files = directory.listFiles();

        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    scanFilesRecursively(file, fileMap);
                } else {
                    String parentPath = file.getAbsolutePath();
                    String fileName = file.getName();
                    fileMap.put(parentPath, fileName);
                }
            }
        }
    }


    private String removeFileExtension(String fileName) {
        File file = new File(fileName);
        int lastDotIndex = file.getName().lastIndexOf('.');

        if (lastDotIndex <= 0) {
            return fileName;
        }

        return file.getName().substring(0, lastDotIndex);
    }
}
