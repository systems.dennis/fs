package net.orivis.file_storage.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.file_storage.form.FileInScopeForm;
import net.orivis.file_storage.model.FileInScopeModel;
import net.orivis.file_storage.service.FileInScopeService;
import net.orivis.shared.annotations.security.Id;
import net.orivis.shared.annotations.security.SelfOnlyRole;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.SelfItemsListController;

@RestController
@RequestMapping("/api/v2/files/file_in_scope")
@OrivisController(value = FileInScopeService.class)
@CrossOrigin
public class FileInScopeController extends OrivisContextable
        implements AddItemController<FileInScopeModel, FileInScopeForm>,
        SelfItemsListController<FileInScopeModel, FileInScopeForm>,
        DeleteItemController<FileInScopeModel> {

    public FileInScopeController(OrivisContext context) {
        super(context);
    }

    @Override
    @WithRole
    @SelfOnlyRole
    public void delete(@Id Long id) throws ItemNotUserException, ItemNotFoundException {
        DeleteItemController.super.delete(id);
    }
}
