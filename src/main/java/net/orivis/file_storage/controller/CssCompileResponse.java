package net.orivis.file_storage.controller;

import lombok.Data;

@Data
public class CssCompileResponse {
    private Boolean exists;
    private String content;
}
