package net.orivis.file_storage.controller;

import lombok.Data;

import java.util.*;

@Data
public class UnifyRequest {
    private String name;

    private boolean force;
    private List<String> unifyJS = new ArrayList<>();

    public List<String> getListOfFiles() {
        return unifyJS.stream().distinct().toList();
    }
}
