package net.orivis.file_storage.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.OrivisSecurityValidator;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.orivis.AllowAllCondition;
import net.orivis.shared.controller.orivis.OrivisRequest;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.file_storage.form.DocumentationForm;
import net.orivis.file_storage.model.DocumentationModel;
import net.orivis.file_storage.service.DocumentationService;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.controller.SearcherInfo;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.entity.TreeNode;
import net.orivis.shared.postgres.controller.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v2/docs")
@OrivisController(value = DocumentationService.class)
@Secured(roles = "ROLE_ADMIN")
@CrossOrigin

public class DocumentationController   extends OrivisContextable
        implements AddItemController<DocumentationModel, DocumentationForm>,
        EditItemController<DocumentationModel, DocumentationForm>,
        ListItemController<DocumentationModel, DocumentationForm>,
        GetByIdController<DocumentationModel, DocumentationForm>,
        DeleteItemController<DocumentationModel> {
    public DocumentationController(OrivisContext context) {
        super(context);
    }

    static {
        SearchEntityApi.registerSearch("docs", new SearcherInfo("title", DocumentationService.class));
    }



    //    @WithRole
    @GetMapping(value = "/tree/parent/{id}", produces = "application/json")
    @ResponseBody
    public ResponseEntity<List<TreeNode>> getByParentId(@PathVariable("id") Long parentId) {

        var data = getService().  findByParent(parentId, 1000, 0)
                .stream()
                .map((x)-> {
                    TreeNode node = new TreeNode();
                    node.setCnt(getService().countByParent(x.getId()));
                    node.setId(x.getId());
                    node.setName(x.getTitle());
                    return node;
                })
                .collect(Collectors.toList());

        return ResponseEntity.ok(data);
    }

    @Override
    @WithRole (ignoreOnCondition =  @OrivisSecurityValidator(AllowAllCondition.class))
    public ResponseEntity<DocumentationForm> get(Long id) {
        return GetByIdController.super.get(id);
    }

    @WithRole (ignoreOnCondition =  @OrivisSecurityValidator( AllowAllCondition.class))
    @GetMapping("/path/{id}")
    public List<KeyValue> getFullPath(@PathVariable Long id){
         var item =  getService().findByIdOrThrow(id);

         if (item == null){
             return Collections.emptyList();
         }

         List<KeyValue> res = new ArrayList<>();

         while (item!= null){
             res.add(new KeyValue(String.valueOf(item.getId()), item.getTitle()));
             item = item.getParent();
         }

         return  res;

    }

    @Override
    public String getDefaultField() {
        return "title";
    }

    @Override
    @WithRole (ignoreOnCondition =  @OrivisSecurityValidator( AllowAllCondition.class))
    public Page<Map<String, Object>> fetchData(OrivisRequest request) {
        return ListItemController.super.fetchData(request);
    }

    //    @WithRole
    @GetMapping(value = "/tree/root", produces = "application/json")
    @ResponseBody
    public ResponseEntity<List<TreeNode>> getRootElements() {

        var data = getService().findRootElements(0, 1000)
                .stream()
                .map(x->{
                    TreeNode node = new TreeNode();
                    node.setCnt(getService().countByRoot(x));
                    node.setId(x.getId());
                    node.setName(x.getTitle());
                    return node;
                })
                .collect(Collectors.toList());

        return ResponseEntity.ok(data);
    }

    @Override
    public DocumentationService getService() {
        return getBean(DocumentationService.class);
    }



}
