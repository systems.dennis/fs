package net.orivis.file_storage.controller;

import lombok.SneakyThrows;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import net.orivis.file_storage.exceptions.FileException;
import net.orivis.shared.exceptions.ItemNotFoundException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v2/js")
@CrossOrigin
public class JSUnifier extends OrivisContextable {

    private static final String UNIFIED_FOLDER = "unified";

    public JSUnifier(OrivisContext context) {
        super(context);
    }

    @SneakyThrows
    @PostMapping("/unify")
    public  String jsUnifier(@RequestBody UnifyRequest request){
        List<String> files = request.getListOfFiles();
        StringBuilder res = new StringBuilder();

        var unifiedFolder = getBaseFilePath();

        String fileName = (request.getName() != null && !request.getName().isEmpty())
                ? request.getName()
                : UUID.randomUUID().toString();
        File targetFile = new File(unifiedFolder, fileName);

        if (targetFile.exists() && !request.isForce()) {
            return targetFile.getName();
        }

        var restTemplate = new RestTemplate();
        restTemplate.getMessageConverters()
                .add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));

        for (String file: files ){
            res.append(update(restTemplate.getForObject(file, String.class))).append("\n");
        }

        Files.writeString(targetFile.toPath(), res.toString());
        return targetFile.getName();
    }

    @SneakyThrows
    @GetMapping("/{id}")
    public String get(@PathVariable  String id){
        File file = new File(getBaseFilePath(), id);
        if (!file.exists()) {
            throw new ItemNotFoundException(id);
        }
        return Files.readString(file.toPath());
    }

    @SneakyThrows
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteFileById(@PathVariable("id") String id) {
        Path filePath = Path.of(getBaseFilePath(), id);

        if (!Files.exists(filePath)) {
            throw new ItemNotFoundException(id);
        }

        try {
            Files.delete(filePath);
            return ResponseEntity.noContent().build();
        } catch (IOException e) {
            throw new FileException("global.app.fs.error.deleting.file", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public String update(String item){
        StringWriter writer = new StringWriter();

        try (BufferedReader reader = new BufferedReader(new StringReader(item))) {
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim().replace("\t", "");
                if (!line.isEmpty() && !line.startsWith("//")) {
                    writer.write(line.replaceAll(" = ", "=" ).replaceAll(" : " , ":" ));
                    writer.write("\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String result = writer.toString();
        if (result.endsWith(System.lineSeparator())) {
            result = result.substring(0, result.length() - System.lineSeparator().length());
        }
        return result;
    }

    private String getBaseFilePath() {
        String basePath = getContext().getEnv("app.global.unified_file_storage", "/opt/file-storage/unified");
        File unifiedFolder = new File(basePath, UNIFIED_FOLDER);
        if (!unifiedFolder.exists()) {
            unifiedFolder.mkdirs();
        }
        return Paths.get(basePath, UNIFIED_FOLDER).toString();
    }
}
