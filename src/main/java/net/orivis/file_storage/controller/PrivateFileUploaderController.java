package net.orivis.file_storage.controller;

import lombok.SneakyThrows;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import net.orivis.file_storage.form.UserFileForm;
import net.orivis.file_storage.model.DownloadResource;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.service.FileInScopeService;
import net.orivis.file_storage.service.FileServicePrivate;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.auth_client.SecurityUtils;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.util.Map;


@RestController ()
@RequestMapping("/api/v2/files/private")
@CrossOrigin
public class PrivateFileUploaderController extends OrivisContextable {


    private final FileServicePrivate service;

    public PrivateFileUploaderController(FileServicePrivate service, OrivisContext context) {
        super(context);
        this.service = service;
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        var file = service.findByIdOrThrow(id);
        getBean(ISecurityUtils.class).isMy(file);
        getBean(FileInScopeService.class).deleteFileInScopeRelation(file);
        service.deleteFilesByUserFile(file);
        service.delete(id);
    }


    @SneakyThrows
    @PostMapping(value = "/upload/{scopeName}", produces = "application/json")
    @WithRole
    public ResponseEntity<UserFileForm> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable String scopeName) {
        try {
            var res = service.save(file);

            ScopeModel scope = getBean(ScopeService.class).findByName(scopeName, null, true);
            getBean(FileInScopeService.class).generateAndSave(res, scope);

            var toRet = getContext().getBean(BeanCopier.class).copy(res, UserFileForm.class);
            Map<String, String> sizes = service.checkFileAndSaveSizesIfItImage(file);
            toRet.setSizes(sizes);

            return ResponseEntity.ok(toRet);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileUploadException("Could not upload the file: " + file.getOriginalFilename() + "!");
        }
    }

    @GetMapping("/download/{name:.+}")
    @ResponseBody
    @WithRole
    public ResponseEntity<UrlResource> getFile(@PathVariable String name) {
        DownloadResource file = service.findByName(name);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getDownloadName() + "\"").contentType(MediaType.valueOf(file.getType())).body(file.getResource());
    }

    @GetMapping("/download/{name:.+}/{size}")
    @ResponseBody
    public ResponseEntity<UrlResource> getFileBySize(@PathVariable String name, @PathVariable int size) {
        DownloadResource file = service.findByNameAndSize(name, size);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getDownloadName() + "\"").contentType(MediaType.valueOf(file.getType())).body(file.getResource());
    }

    @GetMapping(value = "/edit/{name:.+}/{pub}", produces = "application/json")
    @ResponseBody
    @WithRole
    public ResponseEntity<UserFile> setPublic(@PathVariable Boolean pub, @PathVariable String name ) {
        UserFile file = service.setPublic(name, pub);
        return ResponseEntity.ok(file);
    }


    @GetMapping(value = "/search/{scopeName}", produces = "application/json")
    @ResponseBody
    public ResponseEntity<Page<UserFile>> getFiles( @RequestParam(value = "page" , required = false)  Integer page,
                                                    @RequestParam(value = "limit", required = false) Integer limit,
                                                    @RequestParam(value = "s", required = false) String query,
                                                    @RequestParam(value = "onlyImages", required = false) boolean onlyImages,
                                                    @PathVariable String scopeName) {

        return ResponseEntity.ok(service.searchFiles(page == null ? 0 : page, limit, query, onlyImages,true, scopeName));
    }



    @GetMapping(value = "/my/{scopeName}", produces = "application/json")
    @ResponseBody
    @WithRole
    public ResponseEntity<Page<UserFile>> getFiles( @RequestParam(value = "page", required = false)  Integer page,
                                                    @RequestParam(value = "limit", required = false) Integer limit,
                                                    @PathVariable String scopeName) {

        return ResponseEntity.ok(service.findFiles(page == null ? 0 : page, limit, true, scopeName));

    }

    @GetMapping(value = "/versions/{fileName:.+}", produces = "application/json")
    @ResponseBody
    @WithRole
    public ResponseEntity<Page<FileVersion>> getFilesVersions(@PathVariable String fileName,
                                                              @RequestParam(value = "page", required = false)  Integer page,
                                                              @RequestParam(value = "limit", required = false) Integer limit) {

        return ResponseEntity.ok(service.findFileVersions(fileName,  page == null ? 0 : page, limit));

    }

    @SneakyThrows
    @GetMapping(value =  "/download-v/{name:.+}/v/{version}",  produces = "application/json")
    @ResponseBody
    @WithRole
    public ResponseEntity<UrlResource> getFile(@PathVariable String name, @PathVariable Long version) {
        if(service.getFileTypeByName(name).equals("image")) {
            version = version + 1;
        }
        var file = service.findFileVersion( name, version);
        getContext().getBean(SecurityUtils.class).isMy(file);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getDownloadName() + "\"").header("Content-Type" , file.getType()).body(file.getResource());
    }

}
