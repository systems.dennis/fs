package net.orivis.file_storage.controller;


import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.file_storage.form.AccessForm;
import net.orivis.file_storage.model.AccessModel;
import net.orivis.file_storage.service.AccessService;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.SelfItemsListController;

@RestController
@RequestMapping("/api/v2/files/access")
@OrivisController(value = AccessService.class)
@CrossOrigin
public class AccessController extends OrivisContextable
        implements AddItemController<AccessModel, AccessForm>,
        SelfItemsListController<AccessModel, AccessForm>,
        DeleteItemController<AccessModel> {

    public AccessController(OrivisContext context) {
        super(context);
    }
}
