package net.orivis.file_storage.controller;

import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.file_storage.exceptions.FileException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.StandardException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/css")
@CrossOrigin
public class MagicCssParserController extends OrivisContextable {

    private static final String COMPILED_FOLDER = "compiled";

    public MagicCssParserController(OrivisContext context) {
        super(context);
    }

    public Path getCompiledPath() {
        String path = getContext().getEnv("app.global.css_file_storage", "/opt/file-storage/css") + "/" + COMPILED_FOLDER;
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        return directory.toPath();
    }

    @GetMapping(value = "/get/{name}", produces = "text/css")
    public @ResponseBody String getItem(@PathVariable("name") String name, HttpServletResponse response) {
        response.setContentType("text/css");
        Path filePath = getCompiledPath().resolve(name);

        if (!Files.exists(filePath)) {
            throw new ItemNotFoundException(name);
        }

        try {
            return Files.readString(filePath);
        } catch (IOException e) {
            throw new FileException("global.app.fs.error.reading.compiled.css.file", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/exists/{name}")
    public CssCompileResponse exists(@PathVariable("name") String name) {
        CssCompileResponse response = new CssCompileResponse();
        Path filePath = getCompiledPath().resolve(name);
        response.setExists(Files.exists(filePath));
        return response;
    }

    @PostMapping(value = "/compile/{name}", consumes = "application/json")
    public CssCompileResponse precompile(@RequestBody CssCompileRequest text, @PathVariable String name) {
        var res = compile(text.getText(), text.getRoot());

        Path filePath = getCompiledPath().resolve(name);
        try {
            Files.writeString(filePath, res);
        } catch (IOException e) {
            throw new FileException("global.app.fs.error.writing.compiled.css.file", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        CssCompileResponse response = new CssCompileResponse();
        response.setExists(true);
        return response;
    }

    @DeleteMapping("/delete/{name}")
    public ResponseEntity<Void> deleteFile(@PathVariable("name") String name) {
        Path filePath = getCompiledPath().resolve(name);

        if (!Files.exists(filePath)) {
            throw new ItemNotFoundException(name);
        }

        try {
            Files.delete(filePath);
            return ResponseEntity.noContent().build();
        } catch (IOException e) {
            throw new FileException("global.app.fs.error.deleting.file", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete-all")
    public ResponseEntity<Void> deleteAllFiles() {
        Path compiledPath = getCompiledPath();

        try (var filesStream = Files.list(compiledPath)) {
            filesStream.forEach(file -> {
                try {
                    Files.delete(file);
                } catch (IOException e) {
                    log.warn("global.app.fs.error.deleting.file");
                }
            });
            return ResponseEntity.noContent().build();
        } catch (IOException e) {
            throw new FileException("global.app.fs.error.deleting.all.files", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public String compile(String css, String root) {
        Scanner scanner = new Scanner(css);

        Map<String, String> rules = new HashMap<>();

        List<String> lines = new ArrayList<>();
        while (scanner.hasNext()) {
            var line = scanner.nextLine();
            if (line.trim().startsWith("--rule")) {
                try {
                    var ruleName = line.replace("--rule", "");
                    ruleName = ruleName.substring(0, ruleName.indexOf("[")).trim().replace("--", "");
                    var text = line.substring(line.indexOf("[") + 1, line.indexOf("]"));
                    rules.put(ruleName, text);
                } catch (Exception e) {
                    throw new StandardException(line, "incorrect_rule.");
                }
            } else if (line.contains("@rule")) {
                var ruleName = line.replace("@rule", "").replace(';', ' ').trim();
                lines.add(rules.get(ruleName.replace("--", "")));

            } else {
                if (!line.trim().isEmpty())
                    lines.add(line.replaceAll("\t", "").replaceAll("--root", root).replaceAll(" {2}", " "));
            }
        }

        return Strings.join(lines, ' ');
    }
}
