package net.orivis.file_storage.controller;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.exceptions.FileException;
import net.orivis.file_storage.form.UserFileForm;
import net.orivis.file_storage.model.DownloadResource;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.service.FileInScopeService;
import net.orivis.file_storage.service.FileServicePublic;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.auth_client.SecurityUtils;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.util.Map;


@RestController()
@RequestMapping("/api/v2/files/public")
@CrossOrigin
@Slf4j
public class PublicFileUploaderController extends OrivisContextable {

    static {
        OrivisContextable.CREATED_FIELDS_MAP.put(UserFile.class, BeanCopier.findField("userDataId", UserFile.class));
    }

    private final FileServicePublic service;

    public PublicFileUploaderController(FileServicePublic service, OrivisContext context) {
        super(context);
        this.service = service;
    }


    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        var file = service.findByIdOrThrow(id);
        getBean(SecurityUtils.class).isMy(file);
        getBean(FileInScopeService.class).deleteFileInScopeRelation(file);
        service.deleteFilesByUserFile(file);
        service.delete(id);
    }

    @SneakyThrows
    @PostMapping(value = "/upload/{scopeName}", produces = "application/json")
    public ResponseEntity<UserFileForm> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable(required = false) String scopeName) {
        if (scopeName == null) {
            scopeName = getBean(ISecurityUtils.class).getToken().getScope();
        }
        try {
            var res = service.save(file);

            ScopeModel scope = getBean(ScopeService.class).findByName(scopeName, null, true);
            getBean(FileInScopeService.class).generateAndSave(res, scope);

            var toRet = getContext().getBean(BeanCopier.class).copy(res, UserFileForm.class);
            try {
                Map<String, String> sizes = service.checkFileAndSaveSizesIfItImage(file);
                toRet.setSizes(sizes);
            } catch (FileException e){
                //ignored
                log.debug("Cannot resize file: " + file.getName() + toRet.getDownloadUrl());

            }

            return ResponseEntity.ok(toRet);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileException("Could not upload the file: " + file.getOriginalFilename() + "!", HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/info")
    public ResponseEntity<UserFileForm> getData(@RequestParam("path") String path) {

        var res = service.findByPath(path);
        return ResponseEntity.ok(getContext().getBean(BeanCopier.class).copy(res, UserFileForm.class));
    }

    @GetMapping("/download/{name:.+}")
    @ResponseBody
    public ResponseEntity<UrlResource> getFile(@PathVariable String name) {
        DownloadResource file = service.findByName(name);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getDownloadName() + "\"").contentType(MediaType.valueOf(file.getType())).body(file.getResource());
    }

    @GetMapping("/download/{name:.+}/{size}")
    @ResponseBody
    public ResponseEntity<UrlResource> getFileBySize(@PathVariable String name, @PathVariable int size) {
        DownloadResource file = service.findByNameAndSize(name, size);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getDownloadName() + "\"").contentType(MediaType.valueOf(file.getType())).body(file.getResource());
    }

    @GetMapping("/edit/pub/{pub}/{name:.+}")
    @ResponseBody
    public ResponseEntity<UserFile> setPublic(@PathVariable(value = "pub") Boolean pub, @PathVariable(value = "name") String name) {
        UserFile file = service.setPublic(name, pub);
        return ResponseEntity.ok(file);
    }


    @GetMapping("/list/{scopeName}")
    @ResponseBody
    public ResponseEntity<Page<UserFile>> getFiles(@RequestParam(value = "page", required = false) Integer page,
                                                   @RequestParam(value = "limit", required = false) Integer limit,
                                                   @PathVariable String scopeName) {

        return ResponseEntity.ok(service.findFiles(page == null ? 0 : page, limit, scopeName));

    }


    @GetMapping(value = "/search/{scopeName}", produces = "application/json")
    @ResponseBody
    public ResponseEntity<Page<UserFile>> getFiles(@RequestParam(value = "page", required = false) Integer page,
                                                   @RequestParam(value = "limit", required = false) Integer limit,
                                                   @RequestParam(value = "s", required = false) String query,
                                                   @RequestParam(value = "onlyImages", required = false) boolean onlyImages,
                                                   @PathVariable String scopeName) {

        return ResponseEntity.ok(service.searchFiles(page == null ? 0 : page, limit, query, onlyImages, false, scopeName));
    }

    @GetMapping(value = "/versions/{fileName:.+}", produces = "application/json")
    @ResponseBody
    public ResponseEntity<Page<FileVersion>> getFilesVersions(@PathVariable String fileName,
                                                              @RequestParam(value = "page", required = false) Integer page,
                                                              @RequestParam(value = "limit", required = false) Integer limit) {

        return ResponseEntity.ok(service.findFileVersions(fileName, page == null ? 0 : page, limit));

    }


    @SneakyThrows
    @GetMapping("/download-v/{name:.+}/v/{version}")
    @ResponseBody
    public ResponseEntity<UrlResource> getFile(@PathVariable String name, @PathVariable Long version) {

        if (service.getFileTypeByName(name).equals("image")) {
            version = version + 1;
        }

        var file = service.findFileVersion(name, version);
        getContext().getBean(SecurityUtils.class).isMy(file);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getDownloadName() + "\"").body(file.getResource());
    }
}
