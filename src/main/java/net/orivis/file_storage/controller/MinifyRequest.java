package net.orivis.file_storage.controller;

import lombok.Data;

import java.io.Serializable;

@Data
public class MinifyRequest implements Serializable {

    private String directory;

    private String destinationFolderName;

    private String originDirectory;

    private String destinationFileAddon;

}
