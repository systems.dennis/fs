package net.orivis.file_storage.controller;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.fs.FileService;
import net.orivis.file_storage.model.CustomMultipartFile;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileVersionRepo;
import net.orivis.file_storage.service.ImageService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v2/files")
@CrossOrigin
public class AddSizesToSavedImagesAPI extends OrivisContextable {

    private final ImageService imageService;

    @Value("${app.global.data_file_storage:.}")
    private String downloadPath;

    public AddSizesToSavedImagesAPI(ImageService imageService, OrivisContext context) {
        super(context);
        this.imageService = imageService;
    }

    @GetMapping("/checkAndAdd")
    public List<UserFile> checkAndAddSizesToSavedImages() {
        var images = imageService.getAllImages();
        List<UserFile> changedFiles = new ArrayList<>();

        for(UserFile userFile : images) {
            try {

                List<FileVersion> versions = imageService.getFileVersions(userFile);
                for(FileVersion version : versions) {

                    getBean(FileVersionRepo.class).save(saveAnyVersion(userFile, version));

                }

                FileVersion firstVersion = saveFirstVersion(userFile);
                if(firstVersion != null) getBean(FileVersionRepo.class).save(firstVersion);
                changedFiles.add(userFile);
            } catch(Exception e) {
                e.printStackTrace();
            }

        }

        return changedFiles;
    }

    private FileVersion saveAnyVersion(UserFile userFile, FileVersion version) throws IOException{

        String oldFileVersionPath = this.downloadPath + getSeparator() + "download-v"
                + getSeparator() + userFile.getName() + getSeparator() +  "v" + getSeparator() + version.getVersion() + ".version";

        boolean isThereOldFile = new File(oldFileVersionPath).exists();
        if(!isThereOldFile) {
            oldFileVersionPath = this.downloadPath + getSeparator() + "download-v"
                    + getSeparator() + userFile.getName() + getSeparator() +  "v" + getSeparator()
                    + "v" + (version.getVersion() - 1) + getSeparator() + (version.getVersion() - 1) + ".version";
            version.setVersion(version.getVersion() - 1);
        }

        InputStream versionStream = new FileInputStream(oldFileVersionPath);
        InputStream streamToBytes = new FileInputStream(oldFileVersionPath);
        byte[] bytes = streamToBytes.readAllBytes();

        String directoryToVersion = getSeparator() + "download-v" + getSeparator()
                + userFile.getName() + getSeparator() + "v" + getSeparator() + "v" + version.getVersion();

        if(new File(this.downloadPath + directoryToVersion + getSeparator()).exists()) deleteVersionSizes(this.downloadPath + directoryToVersion + getSeparator());

        new File(this.downloadPath + directoryToVersion).mkdirs();

        String newFileVersionPath = directoryToVersion + getSeparator() + version.getVersion() + ".version";
        if(!new File(this.downloadPath + newFileVersionPath).exists()) {
            Files.copy(versionStream, Path.of(this.downloadPath + newFileVersionPath));
        }

        var fileToConvert = getContext().getBean(FileService.class).findByIdOrThrow(version.getFile());
        MultipartFile file = convert(bytes, fileToConvert.getOriginalName(), fileToConvert.getType());
        imageService.saveSizesOfImage(file, this.downloadPath + directoryToVersion + getSeparator());

        if(isThereOldFile) {
            new File(oldFileVersionPath).delete();
        }
        version.setVersion(version.getVersion() + 1);
        version.setDownloadPath(newFileVersionPath);

        versionStream.close();
        streamToBytes.close();

        return version;

    }

    private FileVersion saveFirstVersion(UserFile userFile) throws IOException {

        InputStream firstVersionStream = new FileInputStream(this.downloadPath + "/download/" + userFile.getName());
        InputStream firstVersionStreamToBytes = new FileInputStream(this.downloadPath + "/download/" + userFile.getName());
        byte[] bytes = firstVersionStreamToBytes.readAllBytes();
        String directoryToFirstVersion = getSeparator() + "download-v" + getSeparator()
                + userFile.getName() + getSeparator() + "v" + getSeparator() + "v1";
        boolean isThereFirstVersionFile = new File(this.downloadPath + directoryToFirstVersion).exists();

        if(new File(this.downloadPath + directoryToFirstVersion).exists()) deleteVersionSizes(this.downloadPath + directoryToFirstVersion);

        String firstVersionPath = directoryToFirstVersion + getSeparator() + "1.version";
        new File(this.downloadPath + directoryToFirstVersion).mkdirs();


        if(!new File(this.downloadPath + firstVersionPath).exists()) {
            Files.copy(firstVersionStream, Path.of(this.downloadPath + firstVersionPath));
        }

        MultipartFile file = convert(bytes, userFile.getOriginalName(), userFile.getType());
        imageService.saveSizesOfImage(file, this.downloadPath + getSeparator() + directoryToFirstVersion + getSeparator());


        FileVersion firstVersion = createFirstVersion(userFile);
        firstVersion.setDownloadPath(firstVersionPath);
        firstVersion.setUserDataId(userFile.getUserDataId());

        firstVersionStream.close();
        firstVersionStreamToBytes.close();

        if(isThereFirstVersionFile) return null;

        return firstVersion;

    }

    private static void deleteVersionSizes(String directory) {

        new File(directory + getSeparator() + "32x32.version").delete();
        new File(directory + getSeparator() + "64x64.version").delete();
        new File(directory + getSeparator() + "128x128.version").delete();
        new File(directory + getSeparator() + "256x256.version").delete();
        new File(directory + getSeparator() + "512x512.version").delete();

    }

    private static String getSeparator() {
        var separator = FileSystems.getDefault().getSeparator();
        if (separator.equals("\\")) {
            separator = "\\\\";
        }
        return separator;
    }

    private FileVersion createFirstVersion(UserFile file) {
        FileVersion version = new FileVersion();
        version.setVersion(2L);
        version.setFile(file.getId());
        version.setName(file.getName());
        return version;
    }

    public static MultipartFile convert(byte[] bytes, String fileName, String contentType) {
        return new CustomMultipartFile(bytes, fileName, fileName, contentType);
    }

}
