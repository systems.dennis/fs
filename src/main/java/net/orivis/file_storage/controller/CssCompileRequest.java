package net.orivis.file_storage.controller;

import lombok.Data;

@Data
public class CssCompileRequest {
    private String root;
    private String text;
}
