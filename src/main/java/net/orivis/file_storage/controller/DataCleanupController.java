package net.orivis.file_storage.controller;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.file_storage.service.DataCleanupService;
import net.orivis.shared.annotations.security.WithRole;

@RestController
@RequestMapping("/api/v2/files/data_cleanup")
@CrossOrigin
public class DataCleanupController extends OrivisContextable {

    public DataCleanupController(OrivisContext context) {
        super(context);
    }

    @WithRole("ROLE_SYNC")
    @DeleteMapping("/clean/{name}")
    public Boolean findByScope(@PathVariable("name") String scopeName) {
        return getBean(DataCleanupService.class).cleanupData(scopeName);
    }
}
