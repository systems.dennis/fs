package net.orivis.file_storage.validator;

import net.orivis.file_storage.form.AccessForm;
import net.orivis.file_storage.model.AccessMode;
import net.orivis.file_storage.model.AccessModel;
import net.orivis.file_storage.service.AccessService;
import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.pojo_form.ValidationResult;
import net.orivis.shared.validation.ValueValidator;

import java.util.List;
import java.util.Objects;

public class UniqueAccessValidator implements ValueValidator<AccessForm, AccessMode> {
    @Override
    public ValidationResult validate(AccessForm element, AccessMode value, ValidationContent content) {
        try {
            AccessMode.valueOf(element.getMode().name());
        } catch (IllegalArgumentException e) {
            return ValidationResult.fail("global.app.access.mode.not.supported");
        }

        List<AccessModel> existingModels = content.getContext().getBean(AccessService.class).findAccessModelsByFileId(element.getUserFileId());
        boolean isDuplicate = existingModels.stream().anyMatch(accessModel -> {
            if (element.getMode() == AccessMode.PUBLIC || element.getMode() == AccessMode.PRIVATE) {
                return accessModel.getMode() == element.getMode();
            } else {
                return accessModel.getMode() == element.getMode() &&
                        Objects.equals(accessModel.getModel(), element.getModel());
            }
        });

        if (isDuplicate) {
            return ValidationResult.fail("global.app.access.mode.duplicate");
        }

        return ValidationResult.PASSED;
    }
}
