package net.orivis.file_storage.validator;

import net.orivis.file_storage.form.FileInScopeForm;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.service.FileInScopeService;
import net.orivis.file_storage.service.FileServicePublic;
import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.pojo_form.ValidationResult;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;
import net.orivis.shared.validation.ValueValidator;

public class UniqueFileScopeValidator implements ValueValidator<FileInScopeForm, Long> {

    @Override
    public ValidationResult validate(FileInScopeForm element, Long value, ValidationContent content) {
        UserFile userFile = content.getContext().getBean(FileServicePublic.class).findByIdOrThrow(element.getUserFile());
        ScopeModel scope = content.getContext().getBean(ScopeService.class).findByIdOrThrow(element.getScope());

        boolean isDuplicate = content.getContext().getBean(FileInScopeService.class).isRelationExist(userFile, scope);

        if (isDuplicate) {
            return ValidationResult.fail("duplicate.relationship");
        }

        return ValidationResult.PASSED;
    }
}
