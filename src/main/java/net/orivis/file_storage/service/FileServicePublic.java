package net.orivis.file_storage.service;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.SneakyThrows;
import net.orivis.shared.config.OrivisContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.exceptions.FileException;
import net.orivis.file_storage.model.DownloadResource;
import net.orivis.file_storage.model.FileInScopeModel;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileInScopeRepo;
import net.orivis.file_storage.repo.FileRepo;
import net.orivis.file_storage.repo.FileVersionRepo;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

@Service
@DeleteStrategy(value = DeleteStrategy.DELETE_STRATEGY_PROPERTY)
public class FileServicePublic extends PaginationService<UserFile> implements FileService {

    private final FileRepo fileRepo;

    private final ImageService imageService;
    private final FileVersionRepo fileVersionRepo;
    @Value("${app.global.data_file_storage:.}")
    private String downloadPath;

    @SneakyThrows
    public FileServicePublic(OrivisContext context, FileRepo fileRepo, ImageService imageService, FileVersionRepo repo, Environment environment) {
        super(context);
        this.fileRepo = fileRepo;
        this.imageService = imageService;
        this.fileVersionRepo = repo;
        this.downloadPath = environment.getProperty("app.global.data_file_storage", ".");
        Files.createDirectories(Path.of(downloadPath));
        if (!new File(downloadPath).exists() || !new File(downloadPath).isDirectory()) {
            throw new IllegalArgumentException("The selected path is not Directory or not existing " + new File(downloadPath).getAbsolutePath());
        }
    }

    public UserFile save(MultipartFile file) {
        try {
            if (file == null) {
                throw new IllegalArgumentException("Null is not allowed for file");
            }

            var userFile = createUserFile(file);
            userFile.setPub(true);
            var existingPrivate = fileRepo.exists(getFilterImpl().ofUser(UserFile.class,getCurrentUser()).and(getFilterImpl().eq("pub", Boolean.FALSE)).and(getFilterImpl().contains("originalName", file.getOriginalFilename()).setInsensitive(true)));


            if (existingPrivate) {
                throw new FileException("Already exists file with name: " + file.getOriginalFilename()
                        + " and it is private. Delete that file, or rename file and try again. You can also use api /private to add a version to this file", HttpStatus.BAD_REQUEST);
            }


            UserFile existing = fileRepo.filteredFirst(getFilterImpl().ofUser(UserFile.class,getCurrentUser()).and(getFilterImpl().eq("pub", Boolean.TRUE)).and(getNotDeletedQuery())
                    .and(getFilterImpl().contains("originalName", file.getOriginalFilename()).setInsensitive(true))).orElse(null);


            if (Objects.requireNonNull(file.getContentType()).split("/")[0].equals("image")) {


                if (existing == null) {

                    File directory = new File(this.downloadPath + "/download");
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }
                    Files.copy(file.getInputStream(), Path.of(this.downloadPath + "/download").resolve(userFile.getName()));
                    save(userFile);

                }


                UserFile currentFile = existing != null ? existing : userFile;
                if (existing == null) {
                    currentFile.setNewlyCreated(true);
                }

                var fileVersion = getLastVersion(currentFile, fileVersionRepo, getCurrentUser());

                var versionPath = "/download-v/" + currentFile.getName() + "/v/" + "v" + (fileVersion.getVersion() - 1) + "/";
                var fileDownloadPath = versionPath + (fileVersion.getVersion() - 1) + ".version";
                fileVersion.setDownloadPath(fileDownloadPath);

                fileVersion.setName(currentFile.getName());


                var fPath = Path.of(this.downloadPath + versionPath);


                fPath.toFile().mkdirs();

                Files.copy(file.getInputStream(), Path.of(this.downloadPath + fileDownloadPath));
                fileVersionRepo.save(fileVersion);


                return (existing != null ? existing : userFile);

            } else if (existing != null) {

                var fileVersion = getLastVersion(existing, fileVersionRepo, getCurrentUser());

                var versionPath = "/download-v/" + existing.getName() + "/v/";
                var fileDownloadPath = versionPath + fileVersion.getVersion() + ".version";
                fileVersion.setDownloadPath(fileDownloadPath);

                fileVersion.setName(existing.getName());


                var fPath = Path.of(this.downloadPath + versionPath);


                fPath.toFile().mkdirs();

                Files.copy(file.getInputStream(), Path.of(this.downloadPath + fileDownloadPath));
                fileVersionRepo.save(fileVersion);

                return existing;

            }

            new File(this.downloadPath + "/download").mkdirs();
            Files.copy(file.getInputStream(), Path.of(this.downloadPath + "/download").resolve(userFile.getName()));
            return save(userFile);

        } catch (ExpiredJwtException e) {
            throw new FileException("Token expired", HttpStatus.FORBIDDEN);
        } catch (Exception e) {
            throw new FileException("Could not store the file. Error: " + e.getMessage(), HttpStatus.BAD_GATEWAY);
        }
    }


    public void deleteFilesByUserFile(UserFile file) {
        String separator = getSeparator();

        String userFilePath = this.downloadPath + file.getDownloadUrl();
        new File(userFilePath).delete();


        var versions = fileVersionRepo.findAll(getFilterImpl().eq("file", file.getId()));

        for (FileVersion version : versions) {

            if (isImage(file)) {
                deleteImageVersion(version, file);
            } else {
                deleteDocumentVersion(version, file);
            }

        }

        String pathToVersions = this.downloadPath + separator + "download-v" + separator + file.getName() + separator + "v";
        new File(pathToVersions).delete();
        String path = this.downloadPath + separator + "download-v" + separator + file.getName();
        new File(path).delete();
    }

    private void deleteImageVersion(FileVersion version, UserFile file) {
        String separator = getSeparator();

        String path = this.downloadPath + separator + "download-v" + separator + file.getName()
                + separator + "v" + separator + "v" + (version.getVersion() - 1) + separator;
        new File(path + (version.getVersion() - 1) + ".version").delete();
        new File(path + "32x32.version").delete();
        new File(path + "64x64.version").delete();
        new File(path + "128x128.version").delete();
        new File(path + "256x256.version").delete();
        new File(path + "512x512.version").delete();
        new File(path).delete();
        fileVersionRepo.delete(version);
    }

    private void deleteDocumentVersion(FileVersion version, UserFile file) {
        String separator = getSeparator();

        String versionPath = this.downloadPath + separator + "download-v" + separator
                + file.getName() + separator + "v" + separator;
        new File(versionPath + version.getVersion() + ".version").delete();

        fileVersionRepo.delete(version);
    }

    public Map<String, String> checkFileAndSaveSizesIfItImage(MultipartFile file) {
        try {
            if (file.getContentType().split("/")[0].equals("image")) {

                var existing = fileRepo.filteredFirst(getFilterImpl().ofUser(UserFile.class,getCurrentUser()).and(getFilterImpl().eq("pub", Boolean.TRUE))
                        .and(getNotDeletedQuery()).and(getFilterImpl()
                        .contains("originalName", file.getOriginalFilename()).setInsensitive(true))).orElse(null);


                if (existing == null) return Collections.emptyMap();

                var fileVersion = getLastVersion(existing, fileVersionRepo, getCurrentUser());

                var versionPath = "/download-v/" + existing.getName() + "/v/" + "v" + (fileVersion.getVersion() - 2) + "/";


                return imageService.saveSizesOfImage(file, this.downloadPath + versionPath);
            }
            return null;
        } catch (IOException e) {
            throw new FileException("Could not store the sizes of this file. Error: " + e.getMessage(), HttpStatus.BAD_GATEWAY);
        }

    }


    @Override
    public FileRepo getRepository() {
        return fileRepo;
    }


    @SneakyThrows
    public DownloadResource findByName(String filename) {
        try {


            UserFile file =fileRepo.filteredFirst(getFilterImpl().eq("pub", Boolean.TRUE).and(getNotDeletedQuery())
                    .and(getFilterImpl().eq("name", filename).setInsensitive(true))).orElse(null);



            var fileVersions = fileVersionRepo.filteredData(getFilterImpl().eq("file", file), Pageable.unpaged(Sort.by(Sort.Direction.DESC, "version"))).getContent();

            FileVersion fileVersion = null;
            if (!fileVersions.isEmpty()) {
                fileVersion = fileVersions.get(0);
            }

            if (fileVersion != null) {

                return findFileVersion(filename, fileVersion.getVersion());
            }

            UrlResource resource = new UrlResource(Path.of(this.downloadPath + "/download/" + filename).toUri());

            if (resource.exists() || resource.isReadable()) {

                return new DownloadResource(resource, file);
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (
                MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }


    @SneakyThrows
    public DownloadResource findByNameAndSize(String filename, int size) {
        try {


            var file = fileRepo.filteredFirst(getFilterImpl().contains("name", filename).setInsensitive(true).and(getFilterImpl().eq("pub", Boolean.TRUE)
                    .and(getNotDeletedQuery()))).orElseThrow(() -> FileException.getCaseException(filename, fileRepo, getContext()));

            var fileVersions = fileVersionRepo.filteredData(getFilterImpl().eq("file", file)
                    .and(getNotDeletedQuery()), Pageable.unpaged( Sort.by(Sort.Direction.DESC, "version"))).getContent();

            FileVersion fileVersion = null;
            if (!fileVersions.isEmpty()) {
                fileVersion = fileVersions.get(0);
            }

            if (fileVersion != null) {

                return findFileVersionWithSize(filename, fileVersion.getVersion(), size);
            }

            UrlResource resource = new UrlResource(Path.of(this.downloadPath + "/download-v/" + filename + "/v/v" + fileVersion.getVersion() + "/" + size + "x" + size + ".version").toUri());

            if (resource.exists() || resource.isReadable()) {

                return new DownloadResource(resource, file);
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (
                MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }


    @Override
    public Page<UserFile> findFiles(Integer page, Integer limit, boolean onlyMy, String scopeName) {
        return findFiles(page, limit, scopeName);
    }

    @SneakyThrows
    public Page<FileVersion> findFileVersions(String name, Integer page, Integer limit) {

        var file = fileRepo.filteredFirst(getFilterImpl().and(getFilterImpl().eq("pub", Boolean.TRUE)).and(getNotDeletedQuery())
                .and(getFilterImpl().contains("name", name).setInsensitive(true))).orElse(null);



        if (file == null) {
            throw new FileException("This file wasn't found!", HttpStatus.NOT_FOUND);
        }

        if (limit == null || limit == 0) {
            limit = 50;
        }

        return fileVersionRepo.filteredData(getNotDeletedQuery().and(getFilterImpl().eq("file", file.getId())), PageRequest.of(page == null ? 0 : page, limit, Sort.by(Sort.Direction.DESC, "version")));
    }

    @SneakyThrows
    public Page<UserFile> findFiles(Integer page, Integer limit, String scopeName) {
        ScopeModel scope = getBean(ScopeService.class).findByName(scopeName, null, true);


        if (limit == null || limit == 0) {
            limit = 50;
        }

        return getBean(FileInScopeRepo.class).filteredData(getFilterImpl().eq("pub", Boolean.TRUE).setJoinOn(USER_FILE).and(getNotDeletedQuery()), PageRequest.of(page == null ? 0 : page, limit, Sort.by(Sort.Direction.DESC, "id")))
                .map(FileInScopeModel::getUserFile);
    }

    public Page<UserFile> searchFiles(Integer page, Integer limit, String query, boolean onlyImages, boolean onlyMy, String scopeName) {
        ScopeModel scope = getBean(ScopeService.class).findByName(scopeName, null, true);

        var filter = getFilterImpl().eq("pub", Boolean.TRUE).setComplex(true).and(getNotDeletedQuery());

        if (Objects.nonNull(query)) {
            filter.and(getFilterImpl().contains("originalName", query).setJoinOn(USER_FILE));
        }

        if (onlyMy) {
            filter.and(getFilterImpl().ofUser(UserFile.class, getCurrentUser()).setJoinOn(USER_FILE));
        }

        if (onlyImages) {
            filter.contains("type", "image").setJoinOn(USER_FILE).setInsensitive(true);
        }

        if (limit == null || limit == 0) {
            limit = 50;
        }

        return getBean(FileInScopeRepo.class).filteredData(filter, PageRequest.of(page == null ? 0 : page, limit, Sort.by(Sort.Direction.DESC, "id")))
                .map(FileInScopeModel::getUserFile);
    }

    @Override
    public UserFile setPublic(String name, Boolean pub) {

        var spec = getFilterImpl().contains("name", name).setInsensitive(true).and(getFilterImpl().ofUser(UserFile.class, getCurrentUser())).and(getNotDeletedQuery());


        var file = fileRepo.filteredFirst(spec).orElseThrow(() -> FileException.getCaseException(name, fileRepo, getContext()));

        if (file.getPub() != null && pub == file.getPub()) {
            throw new FileException("The file state is the same, nothing to change", HttpStatus.NOT_MODIFIED);
        }
        file.setPub(pub);
        return fileRepo.save(file);

    }

    @SneakyThrows
    public DownloadResource findFileVersion(String name, Long version) {

        var fileSpec =  getFilterImpl().contains("name", name)
                .setInsensitive(true).and(getFilterImpl().eq("pub", Boolean.TRUE)).and(getFilterImpl().eq("name", name).and(getNotDeletedQuery()));

        var file = fileRepo.filteredFirst(fileSpec).orElseThrow(() -> FileException.getCaseException(name, fileRepo, getContext()));

        var fileVersionSpec = getFilterImpl().eq("file", file).and(getFilterImpl().eq("version", version)).and(getNotDeletedQuery());

        var res = fileVersionRepo.filteredFirst(fileVersionSpec).orElseThrow(() -> new ItemNotFoundException(name + " -> v [" + version + "]"));

        UrlResource resource;
        if (file.getType().split("/")[0].equals("image")) {
            resource = new UrlResource(Path.of(this.downloadPath + "/download-v/" + file.getName() + "/v/v" + (res.getVersion() - 1) + "/" + (res.getVersion() - 1) + ".version").toUri());
        } else {
            resource = new UrlResource(Path.of(this.downloadPath + "/download-v/" + file.getName() + "/v/" + version + ".version").toUri());
        }

        return new DownloadResource(resource, file, res);
    }

    @SneakyThrows
    public DownloadResource findFileVersionWithSize(String name, Long version, int size) {

        var fileSpec = getFilterImpl().eq("name", name).and(getFilterImpl().eq("pub", Boolean.TRUE));

        var file = fileRepo.filteredFirst(fileSpec).orElseThrow(() -> FileException.getCaseException(name, fileRepo, getContext()));

       var fileVersionSpec = getFilterImpl().eq("file", file).and(getFilterImpl().eq("version", version));
        var res = fileVersionRepo.filteredFirst(fileVersionSpec).orElseThrow(() -> new ItemNotFoundException(name + " -> v [" + version + "]"));

        if (file.getType().equals("image/svg+xml")) {
            file.setType("image/png");
        }
        UrlResource resource = new UrlResource(Path.of(this.downloadPath + "/download-v/" + file.getName() + "/v/v" + (version - 1) + "/" + size + "x" + size + ".version").toUri());


        return new DownloadResource(resource, file, res);
    }

    public Page<UserFile> findFilesPublic(Integer page) {
        return fileRepo.filteredData(getFilterImpl().eq("pub", true).and(getFilterImpl().ofUser(UserFile.class, getCurrentUser())), PageRequest.of(page == null ? 0 : page, 50, Sort.by(Sort.Direction.DESC, "id")));


    }

    public UserFile findByPath(String path) {
        if (path == null) {
            throw new ItemNotFoundException("path.not.exists");
        }

        var el = path.split("/");
        String fileID = el[el.length - 1];

        //fix for the files with versions
        if (fileID.contains("?")) {
            fileID = fileID.substring(0, fileID.indexOf("?"));
        }
        //fix ATTENTION HERE WE RETURN EVEN DELETED FILES TO MARK THEM
        return fileRepo.filteredFirst(getFilterImpl().eq("name", fileID)).orElseThrow(() -> new ItemNotFoundException("ex"));
    }

    public String getFileTypeByName(String name) {

        if (fileRepo.filteredFirst(getNotDeletedQuery().and(getFilterImpl().eq("name", name))).get().getType().split("/")[0].equals("image")) {
            return "image";
        } else {
            return "file";
        }
    }

    private boolean isImage(UserFile file) {
        return file.getType().split("/")[0].equals("image");
    }

    private static String getSeparator() {
        var separator = FileSystems.getDefault().getSeparator();
        if (separator.equals("\\")) {
            separator = "\\\\";
        }
        return separator;
    }
}
