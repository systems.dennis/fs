package net.orivis.file_storage.service;

import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.exceptions.FileException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class ResizeSvgImageService {

    public void resizeAndSaveSvg(MultipartFile file, String outputPath, int width, int height) {
        try {
            Transcoder transcoder = new PNGTranscoder();
            InputStream inputStream = file.getInputStream();
            FileOutputStream outputStream = new FileOutputStream(outputPath);

            transcoder.addTranscodingHint(PNGTranscoder.KEY_WIDTH, (float) width);
            transcoder.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, (float) height);

            TranscoderInput transcoderInput = new TranscoderInput(inputStream);
            TranscoderOutput transcoderOutput = new TranscoderOutput(outputStream);

            transcoder.transcode(transcoderInput, transcoderOutput);

            inputStream.close();
            outputStream.close();
        } catch (IOException | TranscoderException e) {
            throw new FileException("Error resizing SVG image", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
