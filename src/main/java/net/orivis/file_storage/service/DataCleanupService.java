package net.orivis.file_storage.service;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.stereotype.Service;
import net.orivis.file_storage.model.FileInScopeModel;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileInScopeRepo;
import net.orivis.file_storage.repo.FileRepo;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;

import java.util.List;

@Service
public class DataCleanupService extends OrivisContextable {

    public DataCleanupService(OrivisContext context) {
        super(context);
    }

    public Boolean cleanupData(String scopeName) {
        ScopeModel scope = getBean(ScopeService.class).findByName(scopeName, null, true);

        List<FileInScopeModel> filesInScope = getBean(FileInScopeRepo.class).filteredData(getBean(FileInScopeService.class).getFilterImpl().eq("scope", scope)).getContent();

        for (FileInScopeModel fileInScope : filesInScope) {
           deleteFile(fileInScope.getUserFile());
        }

        return true;
    }

    public void deleteFile(UserFile userFile) {
        deleteFileInScopes(userFile);
        getBean(FileServicePublic.class).deleteFilesByUserFile(userFile);
        getBean(FileRepo.class).delete(userFile);
    }

    private void deleteFileInScopes(UserFile userFile) {
        FileInScopeRepo fileInScopeRepo = getBean(FileInScopeRepo.class);
        List<FileInScopeModel> filesInScope = fileInScopeRepo.filteredData(getBean(FileInScopeService.class).getFilterImpl().eq("userFile", userFile)).getContent();

        fileInScopeRepo.deleteAll(filesInScope);
    }
}
