package net.orivis.file_storage.service;

import lombok.SneakyThrows;
import net.orivis.shared.config.OrivisContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.exceptions.FileException;
import net.orivis.file_storage.model.DownloadResource;
import net.orivis.file_storage.model.FileInScopeModel;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileInScopeRepo;
import net.orivis.file_storage.repo.FileRepo;
import net.orivis.file_storage.repo.FileVersionRepo;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;

@Service
@CrossOrigin(origins = "*")
public class FileServicePrivate extends PaginationService<UserFile> implements FileService {

    private final FileRepo fileRepo;
    private final FileVersionRepo fileVersionRepo;

    private final ImageService imageService;
    @Value("${app.global.data_file_storage:.}")
    private String downloadPath;

    @SneakyThrows
    public FileServicePrivate(OrivisContext context, FileRepo fileRepo, FileVersionRepo repo, ImageService imageService, Environment environment) {
        super(context);
        this.fileRepo = fileRepo;
        this.fileVersionRepo = repo;
        this.imageService = imageService;
        this.downloadPath = environment.getProperty("app.global.data_file_storage", ".");
        Files.createDirectories(Path.of(downloadPath));
        if (!new File(downloadPath).exists() || !new File(downloadPath).isDirectory()) {
            throw new IllegalArgumentException("The selected path is not Directory or not existing " + new File(downloadPath).getAbsolutePath());
        }
    }

    public UserFile save(MultipartFile file) {
        try {
            if (file == null) {
                throw new IllegalArgumentException("Null is not allowed for file");
            }
            var userFile = createUserFile(file);
            if (fileRepo.exists(getFilterImpl().ofUser(UserFile.class, getCurrentUser()).and(getFilterImpl().eq("pub", true)))) {
                throw new FileException("Already exists file with name: " + file.getOriginalFilename() +
                        " and it is PUBLIC. Delete that file, or rename file and try again. You can also use api /public to add a version to this file", HttpStatus.BAD_REQUEST);
            }

            UserFile existing = fileRepo.filteredFirst(getFilterImpl().ofUser(UserFile.class, getCurrentUser()).and(getFilterImpl().eq("pub", true))
                    .and(getFilterImpl().contains("originalName", file.getOriginalFilename()).setInsensitive(true))).orElse(null);


            userFile.setPub(false);


            if (Objects.requireNonNull(file.getContentType()).split("/")[0].equals("image")) {


                if (existing == null) {

                    File directory = new File(this.downloadPath + "/download");
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }
                    Files.copy(file.getInputStream(), Path.of(this.downloadPath + "/download").resolve(userFile.getName()));
                    save(userFile);

                }


                UserFile currentFile = existing != null ? existing : userFile;

                var fileVersion = getLastVersion(currentFile, fileVersionRepo, getCurrentUser());

                var versionPath = "/download-v/" + currentFile.getName() + "/v/" + "v" + (fileVersion.getVersion() - 1) + "/";
                var fileDownloadPath = versionPath + (fileVersion.getVersion() - 1) + ".version";
                fileVersion.setDownloadPath(fileDownloadPath);

                fileVersion.setName(currentFile.getName());


                var fPath = Path.of(this.downloadPath + versionPath);


                fPath.toFile().mkdirs();

                Files.copy(file.getInputStream(), Path.of(this.downloadPath + fileDownloadPath));
                fileVersionRepo.save(fileVersion);


                return (existing != null ? existing : userFile);

            } else if (existing != null) {

                var fileVersion = getLastVersion(existing, fileVersionRepo, getCurrentUser());

                var versionPath = "/download-v/" + existing.getName() + "/v/";
                var fileDownloadPath = versionPath + fileVersion.getVersion() + ".version";
                fileVersion.setDownloadPath(fileDownloadPath);

                fileVersion.setName(existing.getName());


                var fPath = Path.of(this.downloadPath + versionPath);


                fPath.toFile().mkdirs();

                Files.copy(file.getInputStream(), Path.of(this.downloadPath + fileDownloadPath));
                fileVersionRepo.save(fileVersion);

                return existing;

            }

            new File(this.downloadPath + "/download").mkdirs();
            Files.copy(file.getInputStream(), Path.of(this.downloadPath + "/download").resolve(userFile.getName()));
            var res = save(userFile);
            return res;
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    public void deleteFilesByUserFile(UserFile file) {
        String separator = getSeparator();

        String userFilePath = this.downloadPath + file.getDownloadUrl();
        new File(userFilePath).delete();

        var versions = fileVersionRepo.filteredData(getFilterImpl().eq("file", file.getId()));

        for (FileVersion version : versions) {

            if (isImage(file)) {
                deleteImageVersion(version, file);
            } else {
                deleteDocumentVersion(version, file);
            }

        }

        String pathToVersions = this.downloadPath + separator + "download-v" + separator + file.getName() + separator + "v";
        new File(pathToVersions).delete();
        String path = this.downloadPath + separator + "download-v" + separator + file.getName();
        new File(path).delete();
    }

    private void deleteImageVersion(FileVersion version, UserFile file) {
        String separator = getSeparator();

        String path = this.downloadPath + separator + "download-v" + separator + file.getName()
                + separator + "v" + separator + "v" + (version.getVersion() - 1) + separator;
        new File(path + (version.getVersion() - 1) + ".version").delete();
        new File(path + "32x32.version").delete();
        new File(path + "64x64.version").delete();
        new File(path + "128x128.version").delete();
        new File(path + "256x256.version").delete();
        new File(path + "512x512.version").delete();
        new File(path).delete();
        fileVersionRepo.delete(version);
    }

    private void deleteDocumentVersion(FileVersion version, UserFile file) {
        String separator = getSeparator();

        String versionPath = this.downloadPath + separator + "download-v" + separator
                + file.getName() + separator + "v" + separator;
        new File(versionPath + version.getVersion() + ".version").delete();

        fileVersionRepo.delete(version);
    }


    public Map<String, String> checkFileAndSaveSizesIfItImage(MultipartFile file) {
        try {
            if (file.getContentType().split("/")[0].equals("image")) {


                UserFile existing = fileRepo.filteredFirst(getFilterImpl().ofUser(UserFile.class, getCurrentUser()).and(getFilterImpl().eq("pub", true)).contains("originalName", file.getOriginalFilename()).setInsensitive(true)).orElse(null);
                var fileVersion = getLastVersion(existing, fileVersionRepo, getCurrentUser());

                var versionPath = "/download-v/" + existing.getName() + "/v/" + "v" + (fileVersion.getVersion() - 2) + "/";

                return imageService.saveSizesOfImage(file, this.downloadPath + versionPath);
            }
            return null;
        } catch (IOException e) {
            throw new FileException("Could not store the sizes of this file. Error: " + e.getMessage(), HttpStatus.BAD_GATEWAY);
        }

    }

    @Override
    public FileRepo getRepository() {
        return fileRepo;
    }


    @SneakyThrows
    public DownloadResource findByName(String filename) {
        try {

            var file = fileRepo.filteredFirst(getFilterImpl().eq("name", filename).and(getFilterImpl().ofUser(UserFile.class, getCurrentUser()))).orElseThrow(() -> FileException.getCaseException(filename, fileRepo, getContext()));


            var fileVersions = fileVersionRepo.filteredData(getFilterImpl().eq("file", file).getQueryRoot(), Pageable.unpaged(Sort.by(Sort.Direction.DESC, "version"))).getContent();

            FileVersion fileVersion = null;
            if (!fileVersions.isEmpty()) {
                fileVersion = fileVersions.get(0);
            }

            if (fileVersion != null) {

                return findFileVersion(filename, fileVersion.getVersion());
            }

            UrlResource resource = new UrlResource(Path.of(this.downloadPath + "/download/" + filename).toUri());

            if (resource.exists() || resource.isReadable()) {

                return new DownloadResource(resource, file);
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (
                MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @SneakyThrows
    public DownloadResource findByNameAndSize(String filename, int size) {
        try {
            var file = fileRepo.filteredFirst(getFilterImpl().ofUser(UserFile.class,getCurrentUser()).and(getFilterImpl().contains("name", filename).setInsensitive(true))).orElseThrow(() -> FileException.getCaseException(filename, fileRepo, getContext()));

            var fileVersions = fileVersionRepo.filteredData(getFilterImpl().eq("file", file), Pageable.unpaged(Sort.by(Sort.Direction.DESC, "version"))).getContent();

            FileVersion fileVersion = null;
            if (!fileVersions.isEmpty()) {
                fileVersion = fileVersions.get(0);
            }

            if (fileVersion != null) {

                return findFileVersionWithSize(filename, fileVersion.getVersion(), size);
            }

            UrlResource resource = new UrlResource(Path.of(this.downloadPath + "/download-v/" + filename + "/v/v" + fileVersion.getVersion() + "/" + size + "x" + size + ".version").toUri());

            if (resource.exists() || resource.isReadable()) {

                return new DownloadResource(resource, file);
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (
                MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public Page<UserFile> findFiles(Integer page, Integer limit, boolean onlyMy, String scopeName) {
        ScopeModel scope = getBean(ScopeService.class).findByName(scopeName, null, true);

        if (limit == null || limit == 0) {
            limit = 50;
        }

        return getBean(FileInScopeRepo.class).filteredData(getFilterImpl().ofUser(UserFile.class, getCurrentUser()).and(getFilterImpl().eq("scope", scope)), PageRequest.of(page == null ? 0 : page, limit, Sort.by(Sort.Direction.DESC, "id")))
                .map(FileInScopeModel::getUserFile);
    }

    @Override
    public Page<UserFile> searchFiles(Integer page, Integer limit, String query, boolean onlyImages, boolean onlyMy, String scopeName) {
        ScopeModel scope = getBean(ScopeService.class).findByName(scopeName, null, true);

        if (page == null) {
            page = 0;
        }

        if (query == null) {
            query = "";
        }

        if (limit == null || limit == 0) {
            limit = 50;
        }

        var filter = getFilterImpl().contains("originalName", query).setInsensitive(true).and(getFilterImpl().ofUser(UserFile.class,getCurrentUser()));


        if (onlyImages) {
            filter = filter.and(getFilterImpl().startsWith("type", "image").setInsensitive(true)).setJoinOn(USER_FILE);
        }

        return getBean(FileInScopeRepo.class).filteredData(filter

                        ,
                        PageRequest.of(page, limit, Sort.by(Sort.Direction.DESC, "id")))
                .map(FileInScopeModel::getUserFile);
    }


    @Override
    public UserFile setPublic(String name, Boolean pub) {

        var file = fileRepo.filteredFirst(getFilterImpl().contains("name", name).setInsensitive(true).and(getFilterImpl().ofUser(UserFile.class,getCurrentUser()))).orElseThrow(() -> FileException.getCaseException(name, fileRepo, getContext()));

        if (file.getPub() != null && pub == file.getPub()) {
            throw new FileException("The file state is the same, nothing to change", HttpStatus.NOT_MODIFIED);
        }
        file.setPub(pub);
        return fileRepo.save(file);

    }


    @SneakyThrows
    public Page<FileVersion>
    findFileVersions(String name, Integer page, Integer limit) {

        var file = fileRepo.filteredFirst(getFilterImpl().eq("name", name).and(getFilterImpl().ofUser(UserFile.class,getCurrentUser()))).orElseThrow(() -> FileException.getCaseException(name, fileRepo, getContext()));

        if (limit == null || limit == 0) {
            limit = 50;
        }

        return fileVersionRepo.filteredData(getFilterImpl().ofUser(UserFile.class,getCurrentUser()).and(getFilterImpl().eq("file", file)), PageRequest.of(page == null ? 0 : page, limit, Sort.by(Sort.Direction.DESC, "version")));
    }

    @SneakyThrows
    public DownloadResource findFileVersion(String name, Long version) {

        var file = fileRepo.filteredFirst(getFilterImpl().eq("name", name).and(getFilterImpl().ofUser(UserFile.class,getCurrentUser())))
                .orElseThrow(() -> FileException.getCaseException(name, fileRepo, getContext()));

        var res = fileVersionRepo.filteredFirst(getFilterImpl().eq("file", file)
                        .and(getFilterImpl().eq("version", version)))
                .orElseThrow(() -> new ItemNotFoundException(name + " -> v [" + version + "]"));

        UrlResource resource;
        if (file.getType().split("/")[0].equals("image")) {
            resource = new UrlResource(Path.of(this.downloadPath + "/download-v/" + file.getName() + "/v/v" + (res.getVersion() - 1) + "/" + (res.getVersion() - 1) + ".version").toUri());
        } else {
            resource = new UrlResource(Path.of(this.downloadPath + "/download-v/" + file.getName() + "/v/" + version + ".version").toUri());

        }


        return new DownloadResource(resource, file, res);
    }

    @SneakyThrows
    public DownloadResource findFileVersionWithSize(String name, Long version, int size) {

        var file = fileRepo.filteredFirst(getFilterImpl().eq("name", name).and(getFilterImpl().ofUser(UserFile.class,getCurrentUser()))).orElseThrow(() -> FileException.getCaseException(name, fileRepo, getContext()));
        var res = fileVersionRepo.filteredFirst(getFilterImpl().eq("file", file).and(getFilterImpl().eq("version", version)))
                .orElseThrow(() -> new ItemNotFoundException(name + " -> v [" + version + "]"));

        if (file.getType().equals("image/svg+xml")) {
            file.setType("image/png");
        }

        UrlResource resource = new UrlResource(Path.of(this.downloadPath + "/download-v/" + file.getName() + "/v/v" + (version - 1) + "/" + size + "x" + size + ".version").toUri());

        return new DownloadResource(resource, file, res);
    }


    public Page<UserFile> findFilesPrivate(Integer page) {
        return fileRepo.filteredData(getFilterImpl().eq("pub", false).and(getFilterImpl().ofUser(UserFile.class,getCurrentUser())), PageRequest.of(page == null ? 0 : page, 50, Sort.by(Sort.Direction.DESC, "id")));


    }

    public String getFileTypeByName(String name) {

        if (fileRepo.filteredFirst(getFilterImpl().eq("name", name)).orElseThrow(() -> ItemNotFoundException.fromId(name)).getType().split("/")[0].equals("image")) {
            return "image";
        } else {
            return "file";
        }
    }

    private boolean isImage(UserFile file) {
        return file.getType().split("/")[0].equals("image");
    }

    private static String getSeparator() {
        var separator = FileSystems.getDefault().getSeparator();
        if (separator.equals("\\")) {
            separator = "\\\\";
        }
        return separator;
    }

}
