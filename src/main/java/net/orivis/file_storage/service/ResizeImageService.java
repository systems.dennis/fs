package net.orivis.file_storage.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
public class ResizeImageService {

    public void resizeAndSaveImage(MultipartFile file, String outputPath, int width, int height) throws IOException {
        BufferedImage originalImage = ImageIO.read(file.getInputStream());

        int imageType = originalImage.getTransparency() == Transparency.OPAQUE ?
                BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;

        Image resultingImage = originalImage.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        BufferedImage outputImage = new BufferedImage(width, height, imageType);

        outputImage.getGraphics().drawImage(resultingImage, 0, 0, null);
        ImageIO.write(outputImage, file.getContentType().split("/")[1], new File(outputPath));
    }

}
