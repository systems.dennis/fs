package net.orivis.file_storage.service;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.config.OrivisContext;
import org.springframework.stereotype.Service;
import net.orivis.file_storage.model.AccessMode;
import net.orivis.file_storage.model.AccessModel;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileRepo;
import net.orivis.file_storage.repo.FileVersionRepo;
import net.orivis.shared.postgres.service.PaginationService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class FileMigrationService extends PaginationService<UserFile> {

    private final String downloadPath = "/download";
    private final String oldVersionPath = "/download-v";

    public FileMigrationService(OrivisContext holder) {
        super(holder);
    }

    @Transactional
    public void migrate() throws IOException {
        String pathToDownload = getContext().getEnv("app.global.data_file_storage", "/opt/file-storage");

        List<UserFile> files = (List<UserFile>) getContext().getBean(FileRepo.class).findAll();
        for (UserFile file : files) {
            try {
                createAccessModel(file.getId());
                migrateFile(pathToDownload, file);
            } catch (Exception e) {
                log.error("Error processing file " + file.getName() + ": " + e.getMessage());
            }
        }

        new File(pathToDownload + oldVersionPath).delete();
    }

    private void migrateFile(String pathToDownload, UserFile file) throws IOException {
        var path = pathToDownload + downloadPath + "/" + file.getName();
        FileInputStream fileInputStream = new FileInputStream(path);
        new File(path).delete();

        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        Files.copy(fileInputStream, Path.of(path).resolve(file.getName()));

        List<FileVersion> fileVersions = getContext().getBean(FileVersionRepo.class)
                .filteredData(getContext().getFilterProvider().eq("file", file.getId())).getContent();

        if (!fileVersions.isEmpty()) {
            File versionsPath = new File(path + "/versions");
            if (!versionsPath.exists()) {
                versionsPath.mkdirs();
            }
        }

        if (file.getType().contains("image")) {
            migrateImageVersions(pathToDownload, path, file, fileVersions);
        } else {
            migrateOtherVersions(pathToDownload, path, file, fileVersions);
        }
    }

    private void migrateImageVersions(String pathToDownload, String path, UserFile file, List<FileVersion> fileVersions) throws IOException {
        for (FileVersion fileVersion : fileVersions) {
            List<String> splitDownloadPath = Arrays.stream(fileVersion.getDownloadPath().split("/")).toList();
            Long versionNumber = Long.valueOf(splitDownloadPath.get(splitDownloadPath.size() - 1).split("\\.")[0]);
            fileVersion.setVersion(versionNumber);

            File versionsPath = new File(path + "/versions/v" + versionNumber);
            if (!versionsPath.exists()) {
                versionsPath.mkdirs();
            }

            String fullPathToOldVersion = pathToDownload + fileVersion.getDownloadPath();
            String newDownloadVersionPath = downloadPath + "/" + file.getName() + "/versions/v" + versionNumber + "/" + versionNumber + ".version";

            try {
                Files.copy(Path.of(fullPathToOldVersion), Path.of(pathToDownload, newDownloadVersionPath), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                log.error("Error copying main version file " + versionNumber + " for file " + file.getName() + ": " + e.getMessage());
            }
            fileVersion.setDownloadPath(newDownloadVersionPath);
            getContext().getBean(FileVersionRepo.class).save(fileVersion);

            String oldPathToSizes = new File(fullPathToOldVersion).getParent();
            String newPathToSizes = new File(newDownloadVersionPath).getParent();

            // Миграция разных размеров с обработкой ошибок
            migrateVersionSize(pathToDownload, oldPathToSizes, newPathToSizes, "32x32.version");
            migrateVersionSize(pathToDownload, oldPathToSizes, newPathToSizes, "64x64.version");
            migrateVersionSize(pathToDownload, oldPathToSizes, newPathToSizes, "128x128.version");
            migrateVersionSize(pathToDownload, oldPathToSizes, newPathToSizes, "256x256.version");
            migrateVersionSize(pathToDownload, oldPathToSizes, newPathToSizes, "512x512.version");

            deleteOldFiles(fullPathToOldVersion, oldPathToSizes);
        }

        deleteOldDirectory(pathToDownload, file);
    }

    private void migrateVersionSize(String pathToDownload, String oldPathToSizes, String newPathToSizes, String sizeFileName) {
        try {
            Files.copy(Path.of(oldPathToSizes, sizeFileName), Path.of(pathToDownload, newPathToSizes, sizeFileName), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("Error copying size " + sizeFileName + ": " + e.getMessage());
        }
    }

    private void migrateOtherVersions(String pathToDownload, String path, UserFile file, List<FileVersion> fileVersions) throws IOException {
        for (FileVersion fileVersion : fileVersions) {
            List<String> splitDownloadPath = Arrays.stream(fileVersion.getDownloadPath().split("/")).toList();
            Long versionNumber = Long.valueOf(splitDownloadPath.get(splitDownloadPath.size() - 1).split("\\.")[0]);
            fileVersion.setVersion(versionNumber);

            String fullPathToOldVersion = pathToDownload + fileVersion.getDownloadPath();
            String newDownloadVersionPath = downloadPath + "/" + file.getName() + "/versions/" + versionNumber + ".version";
            Files.copy(Path.of(fullPathToOldVersion), Path.of(pathToDownload, newDownloadVersionPath), StandardCopyOption.REPLACE_EXISTING);
            fileVersion.setDownloadPath(newDownloadVersionPath);
            getContext().getBean(FileVersionRepo.class).save(fileVersion);

            new File(fullPathToOldVersion).delete();
        }

        deleteOldDirectory(pathToDownload, file);
    }

    private void deleteOldFiles(String fullPathToOldVersion, String oldPathToSizes) {
        try {
            new File(fullPathToOldVersion).delete();
        } catch (Exception e) {
            log.error("Failed to delete file: " + fullPathToOldVersion + ". Error: " + e.getMessage());
        }

        deleteFileSafely(oldPathToSizes + "/32x32.version");
        deleteFileSafely(oldPathToSizes + "/64x64.version");
        deleteFileSafely(oldPathToSizes + "/128x128.version");
        deleteFileSafely(oldPathToSizes + "/256x256.version");
        deleteFileSafely(oldPathToSizes + "/512x512.version");

        try {
            new File(oldPathToSizes).delete();
        } catch (Exception e) {
            log.error("Failed to delete directory: " + oldPathToSizes + ". Error: " + e.getMessage());
        }
    }

    private void deleteFileSafely(String filePath) {
        try {
            new File(filePath).delete();
        } catch (Exception e) {
            log.error("Failed to delete file: " + filePath + ". Error: " + e.getMessage());
        }
    }

    private void deleteOldDirectory(String pathToDownload, UserFile file) {
        String pathToDelete = pathToDownload + oldVersionPath + "/" + file.getName();
        new File(pathToDelete + "/v").delete();
        new File(pathToDelete).delete();
    }

    private void createAccessModel(Long fileId) {
        AccessModel accessModel = new AccessModel();
        accessModel.setUserFileId(fileId);
        accessModel.setMode(AccessMode.PUBLIC);
        getContext().getBean(AccessService.class).save(accessModel);
    }
}