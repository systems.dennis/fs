package net.orivis.file_storage.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import net.orivis.file_storage.form.DocumentationForm;
import net.orivis.file_storage.model.DocumentationModel;
import net.orivis.file_storage.repo.DocumentationRepo;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.service.AbstractParentalService;

@Service
@DeleteStrategy(value = DeleteStrategy.DELETE_STRATEGY_PROPERTY)
@OrivisService(model = DocumentationModel.class, form = DocumentationForm.class, repo = DocumentationRepo.class)
public class DocumentationService extends PaginationService<DocumentationModel> implements AbstractParentalService<DocumentationModel, Long> {
    public DocumentationService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public Page<DocumentationModel> findByParent(Long parentId, int limit, int page) {
        OrivisFilter<DocumentationModel> specification = getFilterImpl().eq(getParentField(), findByIdOrThrow( updateId( parentId)));

        return getRepository().filteredData(specification.and(getAdditionalSpecification()), PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "order")));
    }

}
