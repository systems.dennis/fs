package net.orivis.file_storage.service;

import com.madgag.gif.fmsware.AnimatedGifEncoder;
import com.madgag.gif.fmsware.GifDecoder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.exceptions.FileException;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
public class ResizeGifImageService {

    public void resizeAndSaveGif(MultipartFile file, String outputPath, int width, int height) {
        try {
            BufferedImage[] resizedFrames = readAndResizeGif(file, width, height);

            writeGif(new File(outputPath), resizedFrames, true);

        } catch(IOException e) {
            throw new FileException("Error resizing GIF image", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    private static BufferedImage[] readAndResizeGif(MultipartFile file, int width, int height) throws IOException {
        GifDecoder decoder = new GifDecoder();
        decoder.read(file.getInputStream());

        int numFrames = decoder.getFrameCount();
        BufferedImage[] resizedFrames = new BufferedImage[numFrames];

        for (int i = 0; i < numFrames; i++) {
            BufferedImage originalFrame = decoder.getFrame(i);
            BufferedImage resizedFrame = resizeImage(originalFrame, width, height);
            resizedFrames[i] = resizedFrame;
        }

        return resizedFrames;
    }

    private static void writeGif(File outputFile, BufferedImage[] frames, boolean loop) throws IOException {
        AnimatedGifEncoder gifEncoder = new AnimatedGifEncoder();
        gifEncoder.start(outputFile.getPath());
        gifEncoder.setRepeat(loop ? 0 : 1);

        for (BufferedImage frame : frames) {
            gifEncoder.addFrame(frame);
        }

        gifEncoder.finish();
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) {
        BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
        resizedImage.createGraphics().drawImage(originalImage, 0, 0, targetWidth, targetHeight, null);
        return resizedImage;
    }
}
