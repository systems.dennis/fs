package net.orivis.file_storage.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import net.orivis.file_storage.form.FileInScopeForm;
import net.orivis.file_storage.model.FileInScopeModel;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileInScopeRepo;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.scopes.model.ScopeModel;

import java.util.List;

@Service
@OrivisService(model = FileInScopeModel.class, form = FileInScopeForm.class, repo = FileInScopeRepo.class)
@DeleteStrategy(value = DeleteStrategy.DELETE_STRATEGY_PROPERTY)
public class FileInScopeService extends PaginationService<FileInScopeModel> {

    public FileInScopeService(OrivisContext holder) {
        super(holder);
    }

    public boolean isRelationExist(UserFile userFile, ScopeModel scope) {
        var filter = getFilterImpl().eq("userFile", userFile).and(getFilterImpl().eq("scope", scope));


        return getRepository().filteredCount(filter) > 0;
    }

    public void generateAndSave(UserFile userFile, ScopeModel scope) {
        if (!isRelationExist(userFile, scope)) {
            FileInScopeModel fileInScope = new FileInScopeModel();
            fileInScope.setScope(scope);
            fileInScope.setUserFile(userFile);
            save(fileInScope);
        }
    }

    public void deleteFileInScopeRelation(UserFile userFile) {
        List<FileInScopeModel> models = getRepository().filteredData(getFilterImpl().eq("userFile", userFile), Pageable.unpaged()).getContent();

        for (FileInScopeModel model : models) {
            getRepository().delete(model);
        }
    }
}
