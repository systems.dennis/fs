package net.orivis.file_storage.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import net.orivis.file_storage.exceptions.FileException;
import net.orivis.file_storage.form.AccessForm;
import net.orivis.file_storage.fs.FileService;
import net.orivis.file_storage.model.AccessMode;
import net.orivis.file_storage.model.AccessModel;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.AccessModelRepo;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;

import java.util.List;
import java.util.Objects;

@Service
@OrivisService(model = AccessModel.class, form = AccessForm.class, repo = AccessModelRepo.class)
@DeleteStrategy(value = DeleteStrategy.DELETE_STRATEGY_PROPERTY)
public class AccessService extends PaginationService<AccessModel> {
    public AccessService(OrivisContext holder) {
        super(holder);
    }

    public void saveDefaultAccessModel(Long userFileId) {
        AccessModel accessModel = new AccessModel();
        accessModel.setMode(AccessMode.PUBLIC);
        accessModel.setUserFileId(userFileId);
        save(accessModel);
    }

    public void saveAccessModel(Long userFileId, AccessModel accessModel) {
        if (accessModel == null) {
            saveDefaultAccessModel(userFileId);
        } else {
            accessModel.setUserFileId(userFileId);

            switch (accessModel.getMode()) {
                case PUBLIC:
                    accessModel.setModel(null);
                    updateFilePubStatus(userFileId, true);
                    break;
                case PRIVATE, TEAM:
                    accessModel.setModel(null);
                    updateFilePubStatusIfNecessary(userFileId);
                    break;
                case SCOPE:
                    accessModel.setModel(getScopeId());
                    updateFilePubStatusIfNecessary(userFileId);
                    break;
                case USER:
                    if (accessModel.getModel() == null) {
                        throw new FileException("error.user_id_required_for_user_access_mode", HttpStatus.BAD_REQUEST);
                    }
                    updateFilePubStatusIfNecessary(userFileId);
                    break;
            }

            getRepository().save(accessModel);
        }
    }

    public List<AccessModel> findAccessModelsByFileId(Long userFileId) {
        return filteredData(getFilterImpl().eq("userFileId", userFileId), Pageable.unpaged()).getContent();
    }

    @Override
    public AccessModel afterDelete(AccessModel object) {
        updateFilePubStatusIfNecessary(object.getUserFileId());
        return super.afterDelete(object);
    }

    @Override
    public AccessModel afterAdd(AccessModel object) {
        updateFilePubStatusIfNecessary(object.getUserFileId());
        return super.afterAdd(object);
    }

    private void updateFilePubStatus(Long userFileId, boolean isPublic) {
        UserFile userFile = getBean(FileService.class).findFileById(userFileId);

        userFile.setPub(isPublic);
        getBean(FileService.class).save(userFile);
    }

    private void updateFilePubStatusIfNecessary(Long userFileId) {
        var publicAccess = filteredFirst(getFilterImpl().eq("userFileId", userFileId).and(getFilterImpl().eq("mode" , AccessMode.PUBLIC))).orElse(null);

        updateFilePubStatus(userFileId, publicAccess != null);
    }

    public boolean hasAccess(UserFile userFile) {
        if(userFile.getPub()) {
            return true;
        }

        int accessCount = 0;
        Long userId = 0L;
        try {
            userId = getCurrentUser();
        } catch (Exception ignored){
            //for public files no need to have token
        }
        List<AccessModel> accessModels = findAccessModelsByFileId(userFile.getId());

        if (accessModels.isEmpty()){
            return  true; ///hardfix! change and understand why it doesn't work, on creation or on select
        }

        if(Objects.equals(userFile.getUserDataId(), userId)) {
            return true;
        }

        for (AccessModel accessModel : accessModels) {
            switch (accessModel.getMode()) {
                case PUBLIC:
                    accessCount++;
                    break;
                case PRIVATE:
                    if (Objects.equals(userFile.getUserDataId(), userId)) {
                        accessCount++;
                    }
                    break;
                case USER:
                    if (accessModel.getModel() != null && accessModel.getModel().equals(userId)) {
                        accessCount++;
                    }
                    break;
                case SCOPE:
                    if (accessModel.getModel() != null && Objects.equals(getScopeId(), accessModel.getModel())) {
                        accessCount++;
                    }
                    break;
            }
        }

        return accessCount > 0;
    }

    public Long getScopeId() {
        String scopeName = getBean(ISecurityUtils.class).getToken().getScope();
        ScopeModel scope = getBean(ScopeService.class).findByName(scopeName, null, true);
        return scope.getId();
    }
}
