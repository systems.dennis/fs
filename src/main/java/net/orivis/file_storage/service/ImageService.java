package net.orivis.file_storage.service;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileRepo;
import net.orivis.file_storage.repo.FileVersionRepo;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ImageService extends OrivisContextable {

    private final FileRepo fileRepo;

    private final FileVersionRepo fileVersionRepo;

    private final ResizeImageService resizeImageService;

    private final ResizeGifImageService resizeGifImageService;

    private final ResizeSvgImageService resizeSvgImageService;



    public ImageService(OrivisContext context, FileRepo fileRepo, FileVersionRepo fileVersionRepo, ResizeImageService resizeImageService, ResizeGifImageService resizeGifImageService, ResizeSvgImageService resizeSvgImageService) {
        super(context);
        this.fileRepo = fileRepo;
        this.fileVersionRepo = fileVersionRepo;
        this.resizeImageService = resizeImageService;
        this.resizeGifImageService = resizeGifImageService;
        this.resizeSvgImageService = resizeSvgImageService;
    }

    public Map<String, String> saveSizesOfImage(MultipartFile file, String directory) throws IOException {
        String type = ".version";
        HashMap<String, String> sizes = new HashMap<>();

        sizes.put("32x32", saveImageWithSize(file, directory, 32, 32, type));
        sizes.put("64x64", saveImageWithSize(file, directory, 64, 64, type));
        sizes.put("128x128", saveImageWithSize(file, directory, 128, 128, type));
        sizes.put("256x256", saveImageWithSize(file, directory, 256, 256, type));
        sizes.put("512x512", saveImageWithSize(file, directory, 512, 512, type));

        return sizes;
    }

    private String saveImageWithSize(MultipartFile file, String directory, int width, int height, String type)
            throws IOException {
        String path = directory + width + "x" + height + type;

        if (isSvg(file)) {
            resizeSvgImageService.resizeAndSaveSvg(file, path, width, height);
        } else if (isGif(file)) {
            resizeGifImageService.resizeAndSaveGif(file, path, width, height);
        } else {
            resizeImageService.resizeAndSaveImage(file, path, width, height);
        }

        return "/download-v" + path.split("download-v")[1];
    }

    private static boolean isSvg(MultipartFile file) {
        return file.getContentType().equals("image/svg+xml");
    }

    private static boolean isGif(MultipartFile file) {
        return file.getContentType().equals("image/gif");
    }

    public Iterable<UserFile> getAllImages() {

        return fileRepo.filteredData(getContext().getFilterProvider().contains("type", "image").setInsensitive(true)).getContent();
    }

    public List<FileVersion> getFileVersions(UserFile file) {
        return fileVersionRepo.findAll(getContext().getFilterProvider().eq("file", file)).getContent();
    }

}
