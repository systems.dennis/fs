package net.orivis.file_storage.service;

import net.orivis.shared.config.OrivisContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.model.DownloadResource;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileVersionRepo;

import java.util.Random;
import java.util.UUID;

public interface FileService {
    String USER_FILE = "userFile";

    UserFile save (MultipartFile file);

    Page<FileVersion>
    findFileVersions(String name, Integer page, Integer limit);

    DownloadResource findFileVersion(String name, Long version);


    DownloadResource findByName(String filename);

    OrivisContext getContext();


    default  FileVersion getLastVersion(UserFile existing, FileVersionRepo fileVersionRepo, Long currentUser) {
        try {

            var versions = fileVersionRepo.filteredData(getContext().getFilterProvider().eq("file", existing), Pageable.unpaged(Sort.by(Sort.Direction.DESC, "version"))).getContent();

            FileVersion lastVersion = null;
            if (!versions.isEmpty()) {
                lastVersion = versions.get(0);
            }

            var version = lastVersion != null ? lastVersion.getVersion() : 1;
            FileVersion fileVersion = new FileVersion();
            fileVersion.setUserDataId(currentUser);
            fileVersion.setVersion(version + 1);
            fileVersion.setFile(existing.getId());

            return fileVersion;
        } catch(Exception e) {

            var version = 1L;
            FileVersion fileVersion = new FileVersion();
            fileVersion.setUserDataId(currentUser);
            fileVersion.setVersion(version + 1);
            fileVersion.setFile(existing.getId());

            return fileVersion;
        }

    }

    default String createFileName(){
        String fileExtension =   generateRandomKey("ABCDEF0123456789_", 5);

        String fileName = generateRandomKey("ABCDEF0123456789_", 5) + UUID.randomUUID().toString().substring(0,4);
        return fileName + "." + fileExtension;
    }

    default    String generateRandomKey(String chars, int len) {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString();
    }

    default UserFile createUserFile(MultipartFile file) {
        var uuid = createFileName();

        UserFile userFile = new UserFile();
        userFile.setSize(file.getSize());
        userFile.setName(uuid);
        userFile.setOriginalName(file.getOriginalFilename());
        userFile.setType(file.getContentType());
        userFile.setDownloadUrl("/download/" + uuid);
        return userFile;
    }

    Page<UserFile> findFiles (Integer page, Integer limit, boolean onlyMy, String scopeName);
    Page<UserFile> searchFiles (Integer page, Integer limit, String query, boolean onlyImages,boolean onlyMy, String scopeName);

    UserFile setPublic(String name, Boolean pub);
}