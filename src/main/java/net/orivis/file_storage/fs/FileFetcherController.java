package net.orivis.file_storage.fs;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.OrivisSecurityValidator;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.orivis.AllowAllCondition;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.exceptions.FileException;
import net.orivis.file_storage.form.UserFileForm;
import net.orivis.file_storage.fs.processor.AbstractFileProcessor;
import net.orivis.file_storage.model.AccessMode;
import net.orivis.file_storage.model.AccessModel;
import net.orivis.file_storage.model.DownloadResource;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.service.AccessService;
import net.orivis.file_storage.service.FileInScopeService;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.postgres.controller.SelfItemsListController;
import net.orivis.shared.utils.bean_copier.BeanCopier;

@RestController
@RequestMapping("/api/v2/files")
@Slf4j
@CrossOrigin
@OrivisController(FileService.class)
public class FileFetcherController extends OrivisContextable implements SelfItemsListController<UserFile, UserFileForm> {

    public FileFetcherController(OrivisContext context) {
        super(context);
    }


    @GetMapping(path = {"/info/", "/info"})
    @WithRole(ignoreOnCondition = @OrivisSecurityValidator(AllowAllCondition.class))
    public ResponseEntity<UserFileForm> getData(@RequestParam("path") String path) {
        var res = getBean(FileService.class).findByPath(path);

        if (!getBean(AccessService.class).hasAccess(res)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        return ResponseEntity.ok(getContext().getBean(BeanCopier.class).copy(res, UserFileForm.class));
    }

    @SneakyThrows
    @PostMapping(value = "/upload/{public_access}", produces = "application/json")
    @WithRole
    public ResponseEntity<UserFileForm> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("public_access") Boolean publicAccess) {
        try {

            var processor = AbstractFileProcessor.of(file).setContext(getContext());
            processor.verifyFile();

            FileService fileService = getBean(FileService.class);
            UserFile existingFile = fileService.findExistingFile(file.getOriginalFilename(), getCurrentUser());

            UserFile newUserFile;
            if (processor.isAllowVersioning()) {
                newUserFile = getService().handleFileWithVersioning(existingFile, processor);
            } else {
                newUserFile = getService().handleFileWithoutVersioning(processor);
            }

            if(existingFile == null && publicAccess) {
                var accessMode = new AccessModel();
                accessMode.setMode(AccessMode.PUBLIC);
                getBean(AccessService.class).saveAccessModel(newUserFile.getId(), accessMode);
            }

            return ResponseEntity.ok(toForm(newUserFile));
        } catch (Exception e) {
            log.error("cannot uploadFile", e);
            throw new FileException("global.app.exception_cannot_upload_file", HttpStatus.BAD_REQUEST);
        }
    }

    @SneakyThrows
    @WithRole(ignoreOnCondition = @OrivisSecurityValidator(AllowAllCondition.class))
    @GetMapping("/download-v/{name:.+}/v/{version}")
    @ResponseBody
    public ResponseEntity<UrlResource> getFileByVersion(@PathVariable String name, @PathVariable Long version) {
        UserFile userFile = getService().findFileByName(name);
        if (!getBean(AccessService.class).hasAccess(userFile)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        DownloadResource file = getService().findFileVersion(userFile, version);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getDownloadName() + "\"").body(file.getResource());
    }

    @WithRole(ignoreOnCondition = @OrivisSecurityValidator(AllowAllCondition.class))
    @GetMapping("/download/{name:.+}")
    @ResponseBody
    public ResponseEntity<UrlResource> getFile(@PathVariable String name) {
        UserFile userFile = getService().findFileByName(name);
        if (!getBean(AccessService.class).hasAccess(userFile)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        DownloadResource file = getService().findByFileAndSize(userFile, -1);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getDownloadName() + "\"").contentType(MediaType.valueOf(file.getType())).body(file.getResource());
    }

    @GetMapping("/download/{name:.+}/{size}")
    @WithRole(ignoreOnCondition = @OrivisSecurityValidator(AllowAllCondition.class))
    @ResponseBody
    public ResponseEntity<UrlResource> getFileBySize(@PathVariable String name, @PathVariable int size) {
        UserFile userFile = getService().findFileByName(name);
        if (!getBean(AccessService.class).hasAccess(userFile)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        DownloadResource file = getService().findByFileAndSize(userFile, size);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getDownloadName() + "\"").contentType(MediaType.valueOf(file.getType())).body(file.getResource());
    }

    @WithRole
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteFile(@PathVariable Long id) {
        var file = getService().findFileById(id);
        getBean(FileInScopeService.class).deleteFileInScopeRelation(file);

        if (!getBean(AccessService.class).hasAccess(file)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        var processor = AbstractFileProcessor.of(getService().convert(file)).setContext(getContext());
        processor.deleteFile(file);

        getService().delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public UserFileForm toForm(UserFile form) {
        return  getContext().getBean(BeanCopier.class).copy(form, UserFileForm.class );
    }

    public FileService getService(){
        return  getBean(FileService.class);
    }
}
