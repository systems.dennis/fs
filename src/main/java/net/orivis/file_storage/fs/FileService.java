package net.orivis.file_storage.fs;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.exceptions.FileException;
import net.orivis.file_storage.form.UserFileForm;
import net.orivis.file_storage.fs.processor.AbstractFileProcessor;
import net.orivis.file_storage.model.CustomMultipartFile;
import net.orivis.file_storage.model.DownloadResource;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileRepo;
import net.orivis.file_storage.repo.FileVersionRepo;
import net.orivis.file_storage.service.FileInScopeService;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

@OrivisService(model =  UserFile.class, form =  UserFileForm.class, repo =  FileRepo.class)
@DeleteStrategy (DeleteStrategy.DELETE_STRATEGY_DB)
@Service
@Slf4j
public class FileService extends PaginationService<UserFile> {

    public FileService(OrivisContext context) {
        super(context);

    }

    public UserFile findByPath(String path) {
        if (path == null) {
            throw new ItemNotFoundException("path.not.exists");
        }

        var el = path.split("/");
        String fileID = el[el.length - 1];

        //fix for the files with versions
        if (fileID.contains("?")) {
            fileID = fileID.substring(0, fileID.indexOf("?"));
        }
        //fix ATTENTION HERE WE RETURN EVEN DELETED FILES TO MARK THEM
        return getRepository().filteredFirst(getFilterImpl().eq("name", fileID)).orElseThrow(() -> new ItemNotFoundException("ex"));
    }


    public DownloadResource findByFileAndSize(UserFile file, int size) {
        var existingFileSize = getRealSize(size);
        String addon = "";

        if (existingFileSize != -1){
            addon = "x" + existingFileSize;
        }

        try {
            verify(file);

            var fileStoragePath = getFileStoragePath(file);

            var path = fileStoragePath + "/download/" + file.getName() + "/" + file.getName();
            UrlResource resource = new UrlResource(Path.of(path + addon).toUri());
            UrlResource resourceOrigin = new UrlResource(Path.of(path).toUri());

            if (resource.exists() || resource.isReadable()) {

                return new DownloadResource(resource, file);
            } else {
                return new DownloadResource( resourceOrigin, file);
            }
        } catch (
                MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }

    }

    private void verify(UserFile file) {
        //todo uncomment
        //var scope = getBean(ScopeService.class).findByName( getBean(ISecurityUtils.class).getScope(), getCurrentUser(), false);
        //getBean(FileInScopeService.class).filteredFirst(getFilterImpl().eq("file", file).and(getFilterImpl().eq("scope", scope)));
    }

    private int getRealSize(int size) {

        if (size < 0){
            return  -1;
        }

        if (size <= 16){
            return  16;
        }

        if(size <= 32){
            return  32;
        }

        if(size <= 64){
            return 64;
        }

        if(size <= 128){
            return  128;
        }

        else return 256;

    }

    public UserFile handleFileWithVersioning(UserFile existingFile, AbstractFileProcessor processor) {
        FileService fileService = getBean(FileService.class);
        String scopeName = getBean(ISecurityUtils.class).getToken().getScope();

        if (existingFile == null) {
            var newUserFile = processor.saveFile();
            newUserFile = fileService.saveFile(newUserFile);
            fileService.saveNewVersion(newUserFile, processor);
            fileService.addToScope(newUserFile, scopeName);

            return newUserFile;
        } else {
            fileService.saveNewVersion(existingFile, processor);
            return existingFile;
        }
    }

    public UserFile handleFileWithoutVersioning(AbstractFileProcessor processor) {
        FileService fileService = getBean(FileService.class);
        String scopeName = getBean(ISecurityUtils.class).getToken().getScope();

        var newUserFile = processor.saveFile();
        newUserFile = fileService.saveFile(newUserFile);
        fileService.addToScope(newUserFile, scopeName);
        return newUserFile;
    }

    public UserFile findExistingFile(String originalFilename, Long currentUserId) {
        return getRepository().filteredFirst(
                        getFilterImpl().ofUser(UserFile.class, currentUserId)
                                .and(getNotDeletedQuery())
                                .and(getFilterImpl().contains("originalName", originalFilename).setInsensitive(true)))
                .orElse(null);
    }

    public UserFile saveFile(UserFile userFile) {
        return getRepository().save(userFile);
    }

    public void saveNewVersion(UserFile existingFile, AbstractFileProcessor processor) {
        FileVersion version = processor.saveVersionOnDisk(existingFile);
        getBean(FileVersionRepo.class).save(version);
    }

    public void saveOnDisk(UserFile userFile, AbstractFileProcessor processor) {
        processor.saveOnDisk(userFile);
    }

    public void addToScope(UserFile userFile, String scopeName) {
        ScopeModel scope = getBean(ScopeService.class).findByName(scopeName, null, true);
        getBean(FileInScopeService.class).generateAndSave(userFile, scope);
    }

    @SneakyThrows
    public DownloadResource findFileVersion(UserFile userFile, Long version) {

        var fileVersionSpec = getFilterImpl().eq("file", userFile.getId()).and(getFilterImpl().eq("version", version)).and(getNotDeletedQuery());

        var res = getBean(FileVersionRepo.class).filteredFirst(fileVersionSpec).orElseThrow(() -> new ItemNotFoundException(userFile.getName() + " -> v [" + version + "]"));

        var path = getFileStoragePath(userFile) + "/download/" + userFile.getName() + "/versions";
        UrlResource resource;
        resource = new UrlResource(Path.of(path + "/" + version + ".version").toUri());

        if (resource.exists() || resource.isReadable()) {
            return new DownloadResource(resource, userFile, res);
        } else {
            throw ItemNotFoundException.fromId(userFile.getName());
        }
    }

    private String getFileStoragePath(UserFile file) {
        return file.getType().contains("css")
                ? getContext().getEnv("app.global.css_file_storage", "/opt/file-storage/css")
                : getContext().getEnv("app.global.data_file_storage", "/opt/file-storage");
    }

    public MultipartFile convert(UserFile userFile) {
        try {
            var path = getDefaultPathToUserFile(userFile) + "/" + userFile.getName();
            byte[] bytes = new FileInputStream(path).readAllBytes();

            return new CustomMultipartFile(bytes, userFile.getName(), userFile.getName(), userFile.getType());
        } catch(IOException e) {
            log.error(e.getMessage());
            throw new FileException("Couldn't work with file with name " + userFile.getOriginalName() + "!", HttpStatus.BAD_REQUEST);
        }
    }

    public String getDefaultPathToUserFile(UserFile file) {
        return getFileStoragePath(file) + "/download/" + file.getName();
    }

    public String getFullPathToUserFileVersions(UserFile file) {
        return getFileStoragePath(file) + "/download/" + file.getName() + "/versions";
    }

    public UserFile findFileById(Long id) {
        var spec = getBean(ISecurityUtils.class).belongsToMeSpecification(UserFile.class, getContext()).and(getFilterImpl().id(id)).and(getNotDeletedQuery());
        return getRepository().filteredFirst(spec).orElseThrow(() -> ItemNotFoundException.fromId(id));
    }

    public UserFile findFileByName(String name) {
        return getRepository().filteredFirst(getFilterImpl().eq("name", name).and(getNotDeletedQuery()))
                .orElseThrow(() -> new ItemNotFoundException(name));
    }
}
