package net.orivis.file_storage.fs.processor;

import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.model.UserFile;
import net.orivis.shared.exceptions.StatusException;

public class ApplicationFileProcessor extends AbstractFileProcessor {

    public ApplicationFileProcessor(MultipartFile file) {
        super(file);
    }

    @Override
    public void verifyFile() {
        throw new StatusException("global.app.fs.storing_app.is_not_possible", HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    public UserFile saveFile() {
        return null;
    }

    @Override
    public void deleteFile(UserFile userFile) {

    }
}
