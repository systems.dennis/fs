package net.orivis.file_storage.fs.processor;

import org.springframework.web.multipart.MultipartFile;

public class TextFileProcessor extends MediaFileProcessor {

    public TextFileProcessor(MultipartFile file) {
        super(file);
    }
}
