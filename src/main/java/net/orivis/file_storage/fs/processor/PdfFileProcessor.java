package net.orivis.file_storage.fs.processor;

import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.model.UserFile;

public class PdfFileProcessor extends MediaFileProcessor {

    public PdfFileProcessor(MultipartFile file) {
        super(file);
    }

    @Override
    public String getProcessorName() {
        return "pdf";
    }

    @Override
    public UserFile saveFile() {
        var userFile = createUserFile();
        userFile.setPub(true);
        userFile.setUserDataId(getContext().currentUser());

        saveOnDisk(userFile);

        return userFile;
    }
}
