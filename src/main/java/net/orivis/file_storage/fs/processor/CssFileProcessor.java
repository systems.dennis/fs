package net.orivis.file_storage.fs.processor;

import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileVersionRepo;
import net.orivis.shared.exceptions.StatusException;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;

public class CssFileProcessor extends MediaFileProcessor {

    public CssFileProcessor(MultipartFile file) {
        super(file);
    }

    @Override
    public String getProcessorName() {
        return "css";
    }

    @Override
    public UserFile saveFile() {
        verifyFile();
        var userFile = createUserFile();
        userFile.setPub(true);
        userFile.setUserDataId(getContext().currentUser());

        saveInCssDirectory(userFile);

        return userFile;
    }

    @SneakyThrows
    private void saveInCssDirectory(UserFile userFile) {
        try {
            var cssStoragePath = getContext().getEnv("app.global.css_file_storage", "/opt/file-storage/css") + "/download/" + userFile.getName();
            File directory = new File(cssStoragePath);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            Files.copy(getFile().getInputStream(), Path.of(cssStoragePath).resolve(userFile.getName()));
        } catch (IOException e) {
            throw new StatusException("app.fs.css_file_cannot_be_saved", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @SneakyThrows
    @Override
    public FileVersion saveVersionOnDisk(UserFile userFile) {
        try {
            var cssStoragePath = getContext().getEnv("app.global.css_file_storage", "/opt/file-storage/css");
            var versionPath = cssStoragePath + getSeparator() + "/download/" + userFile.getName() + getSeparator() + "/versions";
            File directory = new File(versionPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            FileVersion version = getLastVersion(userFile, getContext().getBean(FileVersionRepo.class), getContext().currentUser());

            var fileDownloadPath = "/" + version.getVersion() + ".version";
            version.setDownloadPath(versionPath + fileDownloadPath);
            version.setName(userFile.getName());
            var fPath = Path.of(versionPath);

            fPath.toFile().mkdirs();

            Files.copy(getFile().getInputStream(), Path.of(versionPath + fileDownloadPath));

            return version;
        } catch (IOException e) {
            throw new StatusException("app.fs.css_file_version_cannot_be_saved", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
