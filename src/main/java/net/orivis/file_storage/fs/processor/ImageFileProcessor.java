package net.orivis.file_storage.fs.processor;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.imgscalr.Scalr;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.fs.FileService;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileVersionRepo;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
public class ImageFileProcessor extends AbstractFileProcessor {

    public ImageFileProcessor(MultipartFile file) {
        super(file);
    }

    @Override
    public UserFile saveFile() {

        var userFile = createUserFile();
        userFile.setPub(true);
        userFile.setUserDataId(getContext().currentUser());

        saveOnDisk(userFile);

        if (isResizeAllowed()) {
            new Thread(() -> {
                try {
                    resizeImageFile(userFile);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }).start();

        }
        return userFile;
    }

    @SneakyThrows
    private void resizeImageFile(UserFile file) {
        var image = ImageIO.read(getFile().getInputStream());
        saveCopy(file, image,  16);
        saveCopy(file, image, 32);
        saveCopy(file, image ,64);
        saveCopy(file, image, 128);
        saveCopy(file, image, 256);
    }

    private void saveCopy(UserFile file, BufferedImage image, int widthSize) throws IOException {

        var imgResized = getFileFromImage(Scalr.resize(image, widthSize));

        var path = getContext().getEnv("app.global.data_file_storage", "/opt/file-storage") + "/download/" + file.getName()  ;
        Files.copy(imgResized, Path.of(path).resolve(file.getName() + "x" + widthSize));
    }


    @SneakyThrows
    public InputStream getFileFromImage(BufferedImage buffImage) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(buffImage, "png", os);                          // Passing: ​(RenderedImage im, String formatName, OutputStream output)
        return new ByteArrayInputStream(os.toByteArray());
    }

    public boolean isResizeAllowed() {
        return getContext().getEnv("global.app.allow_versioning_" + getProcessorName() + "_" + getFile().getContentType(), true);
    }


    @Override
    public void deleteFile(UserFile userFile) {
        String separator = getSeparator();
        var defaultToUserFile = getContext().getBean(FileService.class).getDefaultPathToUserFile(userFile);
        var fileVersionRepo = getContext().getBean(FileVersionRepo.class);

        if(isAllowVersioning()) {
            var versions = fileVersionRepo.filteredData(getContext().getFilterProvider().eq("file", userFile.getId())).getContent();
            if(!versions.isEmpty()) {
                var fullPathToUserFileVersions = getContext().getBean(FileService.class).getFullPathToUserFileVersions(userFile);

                for (FileVersion version : versions) {

                    String versionPath = fullPathToUserFileVersions + separator + "v" + version.getVersion();
                    new File(versionPath + separator + version.getVersion() + ".version").delete();
                    new File(versionPath).delete();

                    fileVersionRepo.delete(version);
                }

                new File(fullPathToUserFileVersions).delete();
            }
        }

        String fullPathToUserFile = defaultToUserFile + separator + userFile.getName();
        deleteImageFiles(fullPathToUserFile);
        new File(defaultToUserFile).delete();

        deleteFileAccessModels(userFile.getId());
    }

    private static void deleteImageFiles(String fullPathToUserFile) {
        deleteFile(fullPathToUserFile);
        deleteFile(fullPathToUserFile + "x16");
        deleteFile(fullPathToUserFile + "x32");
        deleteFile(fullPathToUserFile + "x64");
        deleteFile(fullPathToUserFile + "x128");
        deleteFile(fullPathToUserFile + "x256");
    }

    private static void deleteFile(String filePath) {
        try {
            File file = new File(filePath);
            if (file.exists() && file.delete()) {
                log.info("File deleted successfully: " + filePath);
            } else {
                log.warn("Failed to delete file: " + filePath);
            }
        } catch (Exception e) {
            log.error("Error occurred while trying to delete file: " + filePath, e);
        }
    }
}
