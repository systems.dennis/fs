package net.orivis.file_storage.fs.processor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.model.UserFile;

public class OfficeFileProcessor extends MediaFileProcessor {

    @Value("${app.global.data_file_storage:.}")
    private String downloadPath;

    public OfficeFileProcessor(MultipartFile file) {
        super(file);
    }

    @Override
    public String getProcessorName() {
        return "office";
    }

    @Override
    public UserFile saveFile() {
        var userFile = createUserFile();
        userFile.setPub(true);
        userFile.setUserDataId(getContext().currentUser());

        saveOnDisk(userFile);

        return userFile;
    }
}
