package net.orivis.file_storage.fs.processor;

import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.fs.FileService;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileVersionRepo;

import java.io.File;

public class MediaFileProcessor extends AbstractFileProcessor {


    public MediaFileProcessor(MultipartFile file) {
        super(file);
    }

    @Override
    public UserFile saveFile() {
        verifyFile();
        var file = createUserFile();
        file.setPub(true);
        file.setUserDataId(getContext().currentUser());
        saveOnDisk(file);
        return file;
    }

    @Override
    public void deleteFile(UserFile userFile) {
        String separator = getSeparator();
        var defaultPathToUserFile = getContext().getBean(FileService.class).getDefaultPathToUserFile(userFile);
        var fileVersionRepo = getContext().getBean(FileVersionRepo.class);

        if(isAllowVersioning()) {
            var versions = fileVersionRepo.filteredData(getContext().getFilterProvider().eq("file", userFile.getId())).getContent();
            if(!versions.isEmpty()) {
                var fullPathToUserFileVersions = getContext().getBean(FileService.class).getFullPathToUserFileVersions(userFile);

                for (FileVersion version : versions) {

                    String versionPath = fullPathToUserFileVersions;
                    new File(versionPath + separator + version.getVersion() + ".version").delete();
                    new File(versionPath).delete();

                    fileVersionRepo.delete(version);
                }

                new File(fullPathToUserFileVersions).delete();
            }
        }

        deleteFileAccessModels(userFile.getId());
        new File(defaultPathToUserFile + separator + userFile.getName()).delete();
        new File(defaultPathToUserFile).delete();
    }
}
