package net.orivis.file_storage.fs.processor;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.config.OrivisContext;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.file_storage.model.AccessModel;
import net.orivis.file_storage.model.FileVersion;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileVersionRepo;
import net.orivis.file_storage.service.AccessService;
import net.orivis.shared.exceptions.StatusException;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Getter
@Slf4j
public abstract class AbstractFileProcessor {

    private final MultipartFile file;

    private OrivisContext context;

    public AbstractFileProcessor(final MultipartFile file){
        this.file = file;
    }

    public String getProcessorName(){
        return "image";
    }
    public boolean isAllowToStore(){
        return getContext().getEnv("global.app.allow_to_store_" + getProcessorName(), true);
    }
    public boolean isAllowVersioning(){
        return getContext().getEnv("global.app.allow_versioning_" + getProcessorName(), false);
    }

    public AbstractFileProcessor setContext(OrivisContext context){
        this.context = context;
        return  this;
    }

    public void verifyFile(){
        if (getFile() == null){
            throw new StatusException("global.fs.file_should_not_be_null", HttpStatus.BAD_REQUEST);
        }
        if (!isAllowToStore()){
            throw new StatusException(("global.fs.file_not_allowed_to_store"), HttpStatus.BAD_REQUEST);
        }
    }

    @SneakyThrows
    public void saveOnDisk(UserFile userFile) {
        try {
            var path = getContext().getEnv("app.global.data_file_storage", "/opt/file-storage") + "/download/" + userFile.getName();
            File directory = new File(path);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            Files.copy(getFile().getInputStream(), Path.of(path).resolve(userFile.getName()));
        } catch (IOException e) {
            log.error("errors saving file", e);
            throw  new StatusException("app.fs.file_cannot_be_saved", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @SneakyThrows
    public FileVersion saveVersionOnDisk(UserFile userFile) {
        try {
            var versionPath = "/download/" + userFile.getName() + "/versions";
            var path = getContext().getEnv("app.global.data_file_storage", "/opt/file-storage") + versionPath;
            File directory = new File(path);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            FileVersion version = getLastVersion(userFile, getContext().getBean(FileVersionRepo.class), getContext().currentUser());

            var fileDownloadPath = "/" + version.getVersion() + ".version";
            version.setDownloadPath(versionPath + fileDownloadPath);
            version.setName(userFile.getName());
            var fPath = Path.of(path);

            fPath.toFile().mkdirs();

            Files.copy(getFile().getInputStream(), Path.of(path + fileDownloadPath));

            return version;
        } catch(IOException e) {
            log.error("errors saving file", e);
            throw new StatusException("app.fs.file_version_cannot_be_saved", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public abstract UserFile saveFile();

    public abstract void deleteFile(UserFile file);

    public UserFile createUserFile() {

        UserFile userFile = new UserFile();
        createFileName(userFile);
        userFile.setSize(file.getSize());

        userFile.setOriginalName(file.getOriginalFilename());
        userFile.setType(file.getContentType());

        //all parts of app should be stored in the folder but not in the root storage folder
        userFile.setDownloadUrl("/download/" + this.updateFilePrivacy(userFile, file) );
        return userFile;
    }

    /**
     * To avoid requesting file type from the file server adding original extension to it
     * @param userFile
     * @param file
     * @return
     */
    public String updateFilePrivacy(UserFile userFile, final MultipartFile file) {
        var res = userFile.getName();
        if (this.getContext().getEnv("global.app.allow_saving_extensions" + getProcessorName(), isImageFile(file.getContentType()))) {
            res += "." + getOriginalExtension();
        }

        userFile.setName(res);


        return res;
    }


    public String getOriginalExtension(){
        try {
            String filename = this.getFile().getOriginalFilename();
            if (filename == null) {
                return "null";
            }

            int lastDotIndex = filename.lastIndexOf(".");
            if (lastDotIndex == -1) {
                return "null";
            }

            return filename.substring(lastDotIndex);
        } catch (NullPointerException e){
            return  "null";
        }
    }

    public void createFileName(UserFile file){

        String fileExtension =   generateRandomKey("ABCDEF0123456789_", 5);

        String fileName = generateRandomKey("ABCDEF0123456789_", 5) + UUID.randomUUID().toString().substring(0,4);
        file.setExtension(fileExtension);
        file.setNameWithExtension(fileName);
        file.setName( fileName + "." + fileExtension);
    }

    public static String generateRandomKey(String chars, int len) {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString();
    }

    public static AbstractFileProcessor of (MultipartFile file){
        var fileType = file.getContentType();
        if (fileType == null) {
            fileType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
        }

        if (isImageFile(fileType)){
            return new ImageFileProcessor(file);
        }

        if (isSvgFile(fileType)){
            return new SvgFileProcessor(file);
        }

        if (isPdfFile(fileType)){
            return new PdfFileProcessor(file);
        }

        if (isMediaType(fileType)){
            return new MediaFileProcessor(file);
        }

        if (isOfficeFile(fileType)){
            return  new OfficeFileProcessor(file);
        }

        if (isCssFile(fileType)) {
            return new CssFileProcessor(file);
        }

        if (isTextFile(fileType)){
            return new TextFileProcessor(file);
        }

        if (isApplicationFile(fileType)){
            return new ApplicationFileProcessor(file);
        }

        if (isMediaFile(fileType)){
            return new MediaFileProcessor(file);
        }

        throw new StatusException("global.fs.unsupported.file", HttpStatus.UNSUPPORTED_MEDIA_TYPE);

    }

    private static boolean isOfficeFile(String fileType) {
        return fileType.contains("vnd");
    }

    static boolean isPdfFile(String fileType) {
        return fileType.contains("pdf");
    }

    static boolean isMediaFile(String fileType){
        return fileType.contains("mp4") || fileType.contains("m4a") || fileType.contains("avi") || fileType.contains("mpg")|| fileType.contains("mpe") || fileType.contains("mpeg") ||
                fileType.contains("mpega") || fileType.contains("mp3") || fileType.contains("m4v") || fileType.contains("m4v2") || fileType.contains("ogg") ;
    }

    static boolean isTextFile(String file) {
        return file.contains("json") || file.contains("xml") || file.contains("text") || file.contains("html") || file.contains("xsl");
    }

    static boolean isCssFile(String fileType) {
        return fileType.contains("css");
    }

    static boolean isMediaType(String file) {
        return file.equals(MediaType.APPLICATION_OCTET_STREAM_VALUE);
    }

    static boolean isApplicationFile(String fileType) {
        return fileType.contains(MediaType.APPLICATION_OCTET_STREAM_VALUE);
    }

    static boolean isSvgFile(String fileType) {
        return fileType.contains("svg");
    }

    static boolean isImageFile(String fileType){
        if (fileType == null){
            return  false;
        }
        return fileType.contains("image") && !fileType.contains("svg");
    }

    public FileVersion getLastVersion(UserFile existing, FileVersionRepo fileVersionRepo, Long currentUser) {
        try {
            var versions = fileVersionRepo.filteredData(getContext().getFilterProvider().eq("file", existing.getId()), Sort.by(Sort.Direction.DESC, "version")).getContent();

            FileVersion lastVersion = null;
            if (!versions.isEmpty()) {
                lastVersion = versions.get(0);
            }

            var version = lastVersion != null ? lastVersion.getVersion() : 0;
            FileVersion fileVersion = new FileVersion();
            fileVersion.setUserDataId(currentUser);
            fileVersion.setVersion(version + 1);
            fileVersion.setFile(existing.getId());

            return fileVersion;
        } catch(Exception e) {

            var version = 0L;
            FileVersion fileVersion = new FileVersion();
            fileVersion.setUserDataId(currentUser);
            fileVersion.setVersion(version + 1);
            fileVersion.setFile(existing.getId());

            return fileVersion;
        }
    }

    public static String getSeparator() {
        var separator = FileSystems.getDefault().getSeparator();
        if (separator.equals("\\")) {
            separator = "\\\\";
        }
        return separator;
    }

    public void deleteFileAccessModels(Long userFileId) {
        List<AccessModel> accessModels = getContext().getBean(AccessService.class).findAccessModelsByFileId(userFileId);
        for (AccessModel accessModel : accessModels) {
            getContext().getBean(AccessService.class).delete(accessModel.getId());
        }
    }
}
