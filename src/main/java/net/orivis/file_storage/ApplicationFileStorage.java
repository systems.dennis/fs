package net.orivis.file_storage;

import net.orivis.shared.beans.OnOrivisStart;
import net.orivis.shared.config.OrivisContext;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import net.orivis.file_storage.model.UserFile;
import net.orivis.file_storage.repo.FileRepo;
import net.orivis.file_storage.service.FileInScopeService;
import net.orivis.shared.auth_client.beans.BasicAuthAoe;
import net.orivis.shared.config.MessageResourceSource;
import net.orivis.shared.controller.ExternalControllerAdvice;
import net.orivis.shared.postgres.repository.PaginationRepositoryImpl;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.repository.ScopeRepo;
import net.orivis.shared.scopes.service.AppSettingsService;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@SpringBootApplication (scanBasePackages = {"net.orivis.file_storage.*", "net.orivis.shared.**",
        "net.orivis.shared.*", "org.springframework.web.*"})

@EnableJpaRepositories (basePackages = {
        "net.orivis.shared.*", "net.orivis.file_storage.*"
},  repositoryBaseClass = PaginationRepositoryImpl.class)

@EntityScan(basePackages = {
        "net.orivis.shared.*", "net.orivis.file_storage.*"
})
@CrossOrigin
public class ApplicationFileStorage {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationFileStorage.class);
    }
    @Autowired
    private MessageResourceSource resourceSource;


    @Aspect
    @Component
    public static class aop extends BasicAuthAoe {


        public aop(OrivisContext context) {
            super( context);

        }
    }


    @Bean
    public CorsWebFilter corsWebFilter() {
        CorsConfiguration corsConfig = new CorsConfiguration();
        corsConfig.setAllowedOrigins(Arrays.asList("null", null, "*"));
        corsConfig.setMaxAge(3600L);
        corsConfig.addAllowedMethod("*");
        corsConfig.addAllowedHeader("Requestor-Type");
        corsConfig.addExposedHeader("X-Get-Header");

        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfig);

        return new CorsWebFilter(source);
    }

    @Bean
    public OnOrivisStart appStart(){
        return context -> {
            if (context.getBean(ScopeRepo.class).count() == 0) {
                ScopeModel scope = new ScopeModel();
                scope.setName("DEFAULT");
                scope.setDate(new Date());

                scope = context.getBean(ScopeRepo.class).save(scope);

                List<UserFile> files = new ArrayList<>();
                context.getBean(FileRepo.class).findAll().forEach(files::add);

                for (UserFile userFile : files) {
                    context.getBean(FileInScopeService.class).generateAndSave(userFile, scope);
                }
            }
            context.getBean(AppSettingsService.class).saveApplicationSettingsIntoDB();
        };
    }


    @Bean @Primary
    public MessageSource messageSource() {
        resourceSource.setBasename("messages");
        resourceSource.setDefaultEncoding("UTF-8");
        return resourceSource;
    }
    @Bean @Primary
    public ExternalControllerAdvice advicer() {
        return new FileControllerAdvice();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
