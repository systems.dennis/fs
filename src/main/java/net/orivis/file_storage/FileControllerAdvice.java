package net.orivis.file_storage;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import net.orivis.shared.controller.OrivisErrorDescription;
import net.orivis.shared.controller.OrivisExceptionController;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import net.orivis.file_storage.exceptions.FileException;
import net.orivis.shared.controller.ExternalControllerAdvice;
import net.orivis.shared.exceptions.AuthorizationNotFoundException;

@Component
@Primary
public class FileControllerAdvice implements ExternalControllerAdvice {
    private final static ObjectMapper mapper = new ObjectMapper();

    @ExceptionHandler({AuthorizationNotFoundException.class})
    @ResponseStatus (HttpStatus.FORBIDDEN)
    public String onNeed(Exception e, WebRequest request) {
        return  e.getMessage();
    }


    @ExceptionHandler({FileException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String onFileException(Exception e, WebRequest request) {
        return  e.getMessage();
    }


    @SneakyThrows
    @Override
    public Object onException(Class<?> exception, Exception e, HttpServletResponse response, WebRequest request,  OrivisExceptionController parent) {
        if (e instanceof FileException){
            response.setStatus(((FileException) e).getCode().value());

        }
        return mapper.writeValueAsString(OrivisErrorDescription.of(e,  ((ServletWebRequest) request).getRequest().getRequestURI()));
    }

}
