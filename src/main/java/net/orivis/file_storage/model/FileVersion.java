package net.orivis.file_storage.model;


import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.Data;

import net.orivis.shared.postgres.model.LongAssignableEntity;

@Data
@Entity
@Table(indexes = {@Index(name = "idx_file", columnList = "file")})
public class FileVersion extends LongAssignableEntity {

    private Long version;
    private String downloadPath;
    private Long file;
    private Long size;
    private String name;

    @Override
    public String asValue() {
        return name;
    }
}
