package net.orivis.file_storage.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.postgres.model.LongAssignableEntity;

@Data
@Entity
public class AccessModel extends LongAssignableEntity {
    private Long model;

    private AccessMode mode;

    private Long userFileId;

    @Override
    public String asValue() {
        return "Access: " + userFileId + " Mode: " + mode;
    }
}
