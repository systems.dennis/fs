package net.orivis.file_storage.model;

import lombok.Data;
import org.springframework.core.io.UrlResource;

@Data
public class DownloadResource {

    private UrlResource resource;
    private String downloadName;
    private Long version;
    private String type;

    public DownloadResource(UrlResource resource){
        this.resource = resource;
        this.downloadName = resource.getFilename();
        this.type ="plain/html";
    }


    public DownloadResource(UrlResource resource, UserFile file){
        this.resource = resource;
        this.downloadName = file.getOriginalName();
        this.type = file.getType();

    }


    public DownloadResource(UrlResource resource, UserFile file, FileVersion version){
        this.resource = resource;
        this.downloadName = "[ " + version.getVersion() + " ] " +  file.getOriginalName()  ;
        this.type = file.getType();

    }





}
