package net.orivis.file_storage.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.postgres.model.LongAssignableEntity;

@Data
@Entity
public class Team extends LongAssignableEntity {
    private String name;

    @Override
    public String asValue() {
        return name;
    }
}
