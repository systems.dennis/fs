package net.orivis.file_storage.model;

public enum AccessMode {
    PUBLIC,
    PRIVATE,
    USER,
    TEAM,
    SCOPE
}
