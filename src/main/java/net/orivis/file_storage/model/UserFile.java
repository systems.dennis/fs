package net.orivis.file_storage.model;

import jakarta.persistence.Transient;
import lombok.Data;
import net.orivis.shared.annotations.entity.Created;
import net.orivis.shared.postgres.model.LongAssignableEntity;

import jakarta.persistence.Entity;
import java.util.Date;

@Entity
@Data
public class UserFile extends LongAssignableEntity {

    private Long folder;

    @Transient
    private Boolean newlyCreated;

    private String originalName;

    @Created
    private Date created = new Date();

    private Boolean pub = Boolean.FALSE;

    private String name;

    @Transient
    private String extension;

    @Transient
    private String nameWithExtension;

    private String downloadUrl;

    private String type;

    private Long size;

    @Override
    public String asValue() {
        return name;
    }
}
