package net.orivis.file_storage.model;

import lombok.Data;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import net.orivis.shared.postgres.model.LongAssignableEntity;

import java.util.Date;

@Entity @Data
public class Folder extends LongAssignableEntity {

    private Long parent;
    private Date created = new Date();

    private Boolean pub = Boolean.FALSE;

    private String name;


    @Override
    public String asValue() {
        return name;
    }
}
