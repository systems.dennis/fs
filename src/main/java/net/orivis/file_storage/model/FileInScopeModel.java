package net.orivis.file_storage.model;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import net.orivis.file_storage.service.FileServicePublic;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.postgres.model.LongAssignableEntity;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

@Entity
@Data
public class FileInScopeModel extends LongAssignableEntity {

    @ObjectByIdPresentation
    @ManyToOne
    @OrivisTranformer(additionalClass = FileServicePublic.class, transformWith = OrivisIdToObjectTransformer.class)
    private UserFile userFile;

    @ObjectByIdPresentation
    @ManyToOne
    @OrivisTranformer(additionalClass = ScopeService.class, transformWith = OrivisIdToObjectTransformer.class)
    private ScopeModel scope;

    @Override
    public String asValue() {
        return "";
    }
}
