package net.orivis.file_storage.model;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import net.orivis.file_storage.service.DocumentationService;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.annotations.entity.Updated;
import net.orivis.shared.postgres.model.LongAssignableEntity;
import net.orivis.shared.utils.bean_copier.DateToUTCConverter;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

import java.util.Date;
import java.util.List;

@Entity @Data
public class DocumentationModel extends LongAssignableEntity {
    private String title;

    @ManyToOne
    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = DocumentationService.class)
    private DocumentationModel parent;

    @Column (columnDefinition = "text")
    private String text;

    @Column(name = "c_order")
    private Integer order;

    @OrivisTranformer(transformWith = DateToUTCConverter.class)
    @Updated
    private Date lastUpdated;


    @ElementCollection
    private List<String> attachments;


    @Override
    public String asValue() {
        return title;
    }
}
