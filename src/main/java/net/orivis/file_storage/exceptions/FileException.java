package net.orivis.file_storage.exceptions;

import net.orivis.shared.config.OrivisContext;
import org.springframework.http.HttpStatus;
import net.orivis.file_storage.repo.FileRepo;
import net.orivis.shared.exceptions.AccessDeniedException;
import net.orivis.shared.exceptions.StatusException;


public class FileException extends StatusException {


    public FileException(String s, HttpStatus code) {
        super(s, code);
    }

    public static RuntimeException getCaseException (String file, FileRepo repo, OrivisContext context){

        var nameSpec = context.getFilterProvider().eq("name", file);

        if (repo.exists(nameSpec)){

            var accessSpec = context.getFilterProvider().contains("name", file).setInsensitive(true)
                    .and(context.getFilterProvider().eq("pub", Boolean.TRUE))
                            .and(context.getFilterProvider().notEq("userDataId", context.getCurrentUser()));

            if ( repo.exists(accessSpec)){
                return new AccessDeniedException("This file [" + file + "] is not public, and doesn't belong to you.");
            }



            return new StatusException("This file doesn't belong to you : " + file, HttpStatus.FORBIDDEN);

        } else {
            return new StatusException("File not found with name: " + file, HttpStatus.NOT_FOUND);
        }


    }
}
