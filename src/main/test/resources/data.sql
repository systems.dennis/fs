DELETE FROM access_model;
DELETE FROM file_version;
DELETE FROM file_in_scope_model;
DELETE FROM user_file;

INSERT INTO user_file(id, user_data_id, created, download_url, folder, name, original_name, pub, size, type)
VALUES (1, 100, '2024-08-13 17:33:32.035', '/download/4075_2540.FE793', null, '4075_2540.FE793', 'Untitled_logo_2_free-file.jpg', true, 3552, 'image/jpeg'),
        (2, 101, '2024-08-13 17:36:32.035', '/download/1500_2540.FG800', null, '1500_2540.FG800', 'free-file.jpg', true, 4500, 'image/png'),
        (3, 101, '2024-08-13 17:36:32.035', '/download/0FDA1d41c.61B84', null, '0FDA1d41c.61B84', 'text-file.txt', true, 2547, 'text/plain');

INSERT INTO access_model(id, user_data_id, mode, model, user_file_id)
VALUES (1, 100, 0, null, 1),
        (2, 101, 1, null, 2),
        (3, 101, 1, null, 3);

INSERT INTO file_version(id, user_data_id, download_path, file, name, size, version)
VALUES (1, 101, '/download/0FDA1d41c.61B84/versions/2.version', 3, '0FDA1d41c.61B84', 2800, 2);
