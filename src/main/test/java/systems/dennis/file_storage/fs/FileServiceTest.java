package systems.dennis.file_storage.fs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;
import systems.dennis.file_storage.model.DownloadResource;
import systems.dennis.file_storage.model.UserFile;
import systems.dennis.file_storage.repo.FileRepo;
import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.beans.AbstractDataFilterProvider;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.repository.AbstractDataFilter;

import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FileServiceTest {

    @Mock
    private FileRepo fileRepo;

    @Mock
    private WebContext.LocalWebContext context;

    @Mock
    private AbstractDataFilter<UserFile> filter;

    @Mock
    private AbstractDataFilterProvider provider;

    @Spy
    @InjectMocks
    private FileService fileService;

    @Mock
    private ISecurityUtils iSecurityUtils;

    @BeforeEach
    void setUp() {
        when(fileService.getContext()).thenReturn(context);
    }

    @Test
    void testFindByPath_Success() {
        String path = "/download/4075_25f40.FE793/4075_25f40.FE793";
        String fileName = "4075_25f40.FE793";
        UserFile mockFile = new UserFile();
        mockFile.setName(fileName);

        when(fileService.getRepository()).thenReturn(fileRepo);
        when(context.getBean(AbstractDataFilterProvider.class)).thenReturn(provider);
        when(fileService.getFilterImpl()).thenReturn(filter);
        when(fileRepo.filteredFirst(eq(filter))).thenReturn(Optional.of(mockFile));
        when(filter.eq("name", fileName)).thenReturn(filter);

        UserFile result = fileService.findByPath(path);

        assertNotNull(result);
        assertEquals(fileName, result.getName());
    }

    @Test
    void testFindByPath_FileNotFound() {
        String path = "/download/nonexistent.txt";

        when(fileService.getRepository()).thenReturn(fileRepo);
        when(context.getBean(AbstractDataFilterProvider.class)).thenReturn(provider);
        when(fileService.getFilterImpl()).thenReturn(filter);
        when(fileRepo.filteredFirst(any())).thenReturn(Optional.empty());

        assertThrows(ItemNotFoundException.class, () -> fileService.findByPath(path));
    }

//    @Test
//    void testFindByNameAndSize_Success() throws Exception {
//        UserFile file = new UserFile();
//        file.setName("4075_2540.FE793");
//
//        when(fileService.getRepository()).thenReturn(fileRepo);
//        when(context.getBean(AbstractDataFilterProvider.class)).thenReturn(provider);
//        when(fileService.getFilterImpl()).thenReturn(filter);
//        when(filter.eq("name", file.getName())).thenReturn(filter);
//        when(filter.and(any())).thenReturn(filter);
//        when(fileRepo.filteredFirst(any())).thenReturn(Optional.of(file));
//
//        when(fileService.findFileByName("4075_2540.FE793")).thenReturn(file);
//        UrlResource mockResource = PowerMockito.mock(UrlResource.class);
//        PowerMockito.whenNew(UrlResource.class).withAnyArguments().thenReturn(mockResource);
//
//        when(mockResource.exists()).thenReturn(true);
//        when(mockResource.exists()).thenReturn(true);
//
//        when(context.getEnv("app.global.data_file_storage", "/opt/file-storage")).thenReturn("/opt/file-storage");
//        when(fileService.findByNameAndSize("4075_2540.FE793", 32)).thenReturn(new DownloadResource(mockResource, file));
//
//        DownloadResource downloadResource = fileService.findByNameAndSize("4075_2540.FE793", 32);
//
//        assertNotNull(downloadResource);
//        assertEquals(file.getDownloadUrl(), downloadResource.getDownloadName());
//        assertEquals(mockResource, downloadResource.getResource());
//    }

    @Test
    void testFindByFileAndSize_ResourceWithoutSize() throws Exception {
        UserFile file = new UserFile();
        file.setName("4075_254ff0.FE793");

        assertEquals(new UrlResource("file:///Users/olegraytsev/Documents/project/fs/null/download/4075_254ff0.FE793/4075_254ff0.FE793"), fileService.findByFileAndSize(file, 32).getResource());
    }

    @Test
    void testFindExistingFile_Success() {
        String originalFilename = "example.txt";
        Long userId = 1L;
        UserFile mockFile = new UserFile();

        when(fileService.getRepository()).thenReturn(fileRepo);
        when(context.getBean(AbstractDataFilterProvider.class)).thenReturn(provider);
        when(fileService.getFilterImpl()).thenReturn(filter);
        when(filter.ofUser(UserFile.class, userId)).thenReturn(filter);
        when(filter.contains("originalName", originalFilename)).thenReturn(filter);
        when(filter.and(any())).thenReturn(filter);
        when(filter.setInsensitive(true)).thenReturn(filter);
        when(fileRepo.filteredFirst(any())).thenReturn(Optional.of(mockFile));

        UserFile result = fileService.findExistingFile(originalFilename, userId);

        assertEquals(mockFile, result);
        verify(fileRepo, times(1)).filteredFirst(any());
    }

    @Test
    void testFindExistingFile_FileNotFound() {
        String originalFilename = "nonexistent.txt";
        Long userId = 1L;

        when(fileService.getRepository()).thenReturn(fileRepo);
        when(context.getBean(AbstractDataFilterProvider.class)).thenReturn(provider);
        when(fileService.getFilterImpl()).thenReturn(filter);
        when(filter.ofUser(UserFile.class, userId)).thenReturn(filter);
        when(filter.contains("originalName", originalFilename)).thenReturn(filter);
        when(filter.and(any())).thenReturn(filter);
        when(filter.setInsensitive(true)).thenReturn(filter);
        when(fileRepo.filteredFirst(any())).thenReturn(Optional.empty());

        UserFile result = fileService.findExistingFile(originalFilename, userId);

        assertNull(result);
    }

//    @Test
//    void testConvert_Success() throws Exception {
//        UserFile userFile = mock(UserFile.class);
//        String fileName = "4075_2540.FE793";
//        String fileType = "text/plain";
//        byte[] fileContent = "Test content".getBytes();
//        String mockPath = "/mocked/path/4075_2540.FE793";
//
//        when(userFile.getName()).thenReturn(fileName);
//        when(userFile.getType()).thenReturn(fileType);
//        when(fileService.getDefaultPathToUserFile(fileName)).thenReturn("/mocked/path");
//
//        FileInputStream mockInputStream = mock(FileInputStream.class);
//        PowerMockito.whenNew(FileInputStream.class).withArguments(anyString())
//                .thenReturn(mockInputStream);
//        when(mockInputStream.readAllBytes()).thenReturn(fileContent);
//
//        MultipartFile result = fileService.convert(userFile);
//
//        assertNotNull(result);
//        assertArrayEquals(fileContent, result.getBytes());
//        assertEquals(fileName, result.getOriginalFilename());
//        assertEquals(fileType, result.getContentType());
//    }

    @Test
    void testFindFileById_Success() {
        Long fileId = 1L;
        UserFile mockFile = new UserFile();

        when(fileService.getRepository()).thenReturn(fileRepo);
        when(context.getBean(AbstractDataFilterProvider.class)).thenReturn(provider);
        when(fileService.getFilterImpl()).thenReturn(filter);

        when(fileService.getBean(ISecurityUtils.class)).thenReturn(iSecurityUtils);
        when(iSecurityUtils.belongsToMeSpecification(any(), any())).thenReturn(filter);
        when(filter.and(any())).thenReturn(filter);

        when(fileRepo.filteredFirst(any())).thenReturn(Optional.of(mockFile));
        UserFile result = fileService.findFileById(fileId);

        assertEquals(mockFile, result);
        verify(fileRepo, times(1)).filteredFirst(any());
    }

    @Test
    void testFindFileById_NotFound() {
        Long fileId = 1L;

        when(fileService.getRepository()).thenReturn(fileRepo);
        when(context.getBean(AbstractDataFilterProvider.class)).thenReturn(provider);
        when(fileService.getFilterImpl()).thenReturn(filter);

        when(fileService.getBean(ISecurityUtils.class)).thenReturn(iSecurityUtils);
        when(iSecurityUtils.belongsToMeSpecification(any(), any())).thenReturn(filter);
        when(filter.and(any())).thenReturn(filter);

        when(fileRepo.filteredFirst(any())).thenReturn(Optional.empty());

        assertThrows(ItemNotFoundException.class, () -> fileService.findFileById(fileId));
    }

    @Test
    void testFindFileByName_Success() {
        String fileName = "4075_2540.FE793";
        UserFile mockFile = new UserFile();

        when(fileService.getRepository()).thenReturn(fileRepo);
        when(context.getBean(AbstractDataFilterProvider.class)).thenReturn(provider);
        when(fileService.getFilterImpl()).thenReturn(filter);
        when(filter.and(any())).thenReturn(filter);
        when(filter.eq("name", fileName)).thenReturn(filter);
        when(fileRepo.filteredFirst(any())).thenReturn(Optional.of(mockFile));

        UserFile result = fileService.findFileByName(fileName);

        assertEquals(mockFile, result);
        verify(fileRepo, times(1)).filteredFirst(any());
    }

    @Test
    void testFindFileByName_NotFound() {
        String fileName = "4075_2540.FE793";

        when(fileService.getRepository()).thenReturn(fileRepo);
        when(context.getBean(AbstractDataFilterProvider.class)).thenReturn(provider);
        when(fileService.getFilterImpl()).thenReturn(filter);
        when(filter.and(any())).thenReturn(filter);
        when(filter.eq("name", fileName)).thenReturn(filter);

        assertThrows(ItemNotFoundException.class, () -> fileService.findFileByName(fileName));
    }
}
