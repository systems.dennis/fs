package systems.dennis.file_storage.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import systems.dennis.file_storage.form.AccessForm;
import systems.dennis.file_storage.fs.FileFetcherController;
import systems.dennis.file_storage.fs.FileService;
import systems.dennis.file_storage.model.AccessMode;
import systems.dennis.file_storage.model.UserFile;
import systems.dennis.file_storage.repo.FileVersionRepo;
import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.auth_client.beans.BasicAuthAoe;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.TokenData;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@Slf4j
@SpringBootTest
public class FileFetcherControllerTest extends TestContainerBase {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private FileService fileService;
    @Autowired
    private FileFetcherController fileFetcherController;
    @MockBean
    private BasicAuthAoe basicAuthAoe;
    @MockBean
    private ISecurityUtils securityUtils;

    @Value("${app.global.data_file_storage}")
    private String path;

    private String tempFilePath;

    @BeforeEach
    public void setUp() {
        Mockito.doNothing().when(basicAuthAoe).beforeAdvice(Mockito.any());

        String fileName = "0FDA1d41c.61B84";
        String directory = path + File.separator + "download" + File.separator + fileName;
        try {
            tempFilePath = path + File.separator + "download";
            Files.createDirectories(Paths.get(directory));
            Files.createFile(Paths.get(directory, fileName));

            fileName = fileName + "x128";
            Files.createFile(Paths.get(directory, fileName));

            directory += File.separator + "versions";
            Files.createDirectories(Paths.get(directory));
            Files.createFile(Paths.get(directory, "2.version"));

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Test
    public void testGetData() throws Exception {
        String path = "/download/4075_2540.FE793/4075_2540.FE793";

        mockMvc.perform(get("/api/v2/files/info").param("path", path))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testGetDataForbidden() throws Exception {
        String path = "/download/1500_2540.FG800/1500_2540.FG800";

        when(fileService.getCurrentUser()).thenReturn(100L);

        mockMvc.perform(get("/api/v2/files/info").param("path", path))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUploadFileWithoutAccess() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.txt", "text/plain", "content".getBytes());

        TokenData tokenData = new TokenData("default", null);

        when(fileFetcherController.getCurrentUser()).thenReturn(100L);
        when(securityUtils.getToken()).thenReturn(tokenData);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/v2/files/upload")
                .file(multipartFile))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testUploadFileWithAccess() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test1.txt", "text/plain", "content".getBytes());
        AccessForm accessForm = new AccessForm();
        accessForm.setMode(AccessMode.USER);
        accessForm.setModel(100L);

        TokenData tokenData = new TokenData("default", null);

        when(fileFetcherController.getCurrentUser()).thenReturn(100L);
        when(securityUtils.getToken()).thenReturn(tokenData);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/v2/files/upload")
                        .file(multipartFile)
                        .content(objectMapper.writeValueAsString(accessForm))
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testGetFileByVersion() throws Exception {
        String fileName = "0FDA1d41c.61B84";
        Long version = 2L;

        TokenData tokenData = new TokenData("default", null);
        when(fileFetcherController.getCurrentUser()).thenReturn(101L);
        when(securityUtils.getToken()).thenReturn(tokenData);

        mockMvc.perform(get("/api/v2/files/download-v/" + fileName + "/v/" + version))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetFileByVersionForbidden() throws Exception {
        String fileName = "0FDA1d41c.61B84";
        Long version = 2L;

        TokenData tokenData = new TokenData("default", null);
        when(fileFetcherController.getCurrentUser()).thenReturn(100L);
        when(securityUtils.getToken()).thenReturn(tokenData);

        mockMvc.perform(get("/api/v2/files/download-v/" + fileName + "/v/" + version))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetFile() throws Exception {
        String fileName = "0FDA1d41c.61B84";

        TokenData tokenData = new TokenData("default", null);
        when(fileFetcherController.getCurrentUser()).thenReturn(101L);
        when(securityUtils.getToken()).thenReturn(tokenData);

        mockMvc.perform(get("/api/v2/files/download/" + fileName))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetFileForbidden() throws Exception {
        String fileName = "0FDA1d41c.61B84";

        TokenData tokenData = new TokenData("default", null);
        when(fileFetcherController.getCurrentUser()).thenReturn(100L);
        when(securityUtils.getToken()).thenReturn(tokenData);

        mockMvc.perform(get("/api/v2/files/download/" + fileName))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetFileBySize() throws Exception {
        String fileName = "0FDA1d41c.61B84";
        int size = 128;

        TokenData tokenData = new TokenData("default", null);
        when(fileFetcherController.getCurrentUser()).thenReturn(101L);
        when(securityUtils.getToken()).thenReturn(tokenData);

        mockMvc.perform(get("/api/v2/files/download/" + fileName + "/" + size))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetFileBySizeForbidden() throws Exception {
        String fileName = "0FDA1d41c.61B84";
        int size = 128;

        TokenData tokenData = new TokenData("default", null);
        when(fileFetcherController.getCurrentUser()).thenReturn(100L);
        when(securityUtils.getToken()).thenReturn(tokenData);

        mockMvc.perform(get("/api/v2/files/download/" + fileName + "/" + size))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteFile() throws Exception {
        Long fileId = 3L;

        TokenData tokenData = new TokenData("default", null);
        when(fileFetcherController.getCurrentUser()).thenReturn(101L);
        when(securityUtils.getToken()).thenReturn(tokenData);
        var spec = fileService.getFilterImpl().eq("userDataId", 101L);
        when(securityUtils.belongsToMeSpecification(any(), any())).thenReturn(spec);

        mockMvc.perform(delete("/api/v2/files/delete/" + fileId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testDeleteFileForbidden() throws Exception {
        Long fileId = 3L;

        TokenData tokenData = new TokenData("default", null);
        when(fileFetcherController.getCurrentUser()).thenReturn(100L);
        when(securityUtils.getToken()).thenReturn(tokenData);
        var spec = fileService.getFilterImpl().eq("userDataId", 100L);
        when(securityUtils.belongsToMeSpecification(any(), any())).thenReturn(spec);

        mockMvc.perform(delete("/api/v2/files/delete/" + fileId))
                .andExpect(status().isNotFound());
    }

    @AfterEach
    public void tearDown() {
        List<UserFile> files = fileService.find();

        for(UserFile file : files) {
            if(file.getName().equals("0FDA1d41c.61B84")) {
                deleteByPath(tempFilePath + File.separator + file.getName() + File.separator + "0FDA1d41c.61B84x128");
                deleteByPath(tempFilePath + File.separator + file.getName() + File.separator + "versions" + File.separator + "2.version");
            }
            deleteByPath(tempFilePath + File.separator + file.getName() + File.separator + file.getName());
            deleteByPath(tempFilePath + File.separator + file.getName() + File.separator + "versions");
            deleteByPath(tempFilePath + File.separator + file.getName());
            log.info("DELETING FILE {}", file.getId());
        }

        deleteByPath(tempFilePath);
    }

    private void deleteByPath(String path) {
        try {
            new File(path).delete();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}