#v0.0.2
FROM openjdk:20-jdk-slim
RUN mkdir -p /usr/app
RUN mkdir -p /opt/app

ENV HOME=/usr/app

WORKDIR /usr/app
ADD . /usr/app
RUN apt update -y
RUN apt-get install wget -y
RUN apt-get install nginx -y
RUN apt-get install git -y

RUN cp ./piplenine/nginx.conf /etc/nginx/

RUN wget https://dlcdn.apache.org/maven/maven-3/3.9.6/binaries/apache-maven-3.9.6-bin.tar.gz

RUN git clone https://gitlab.com/systems.dennis/fs.git

RUN tar -xf apache-maven-3.9.6-bin.tar.gz
ENV MVN=apache-maven-3.9.6/bin

RUN $MVN/mvn -f /usr/app/fs/pom.xml clean install --settings $HOME/.pipeline/settings.xml

RUN rm $HOME/fs/target/*original*
RUN cp $HOME/fs/target/*.jar /opt/app/app.jar
RUN cp /usr/app/.pipeline/application.properties_server /opt/app/application.properties

#ENTRYPOINT   ["java","-Dspring.config.location=/opt/app/", "-jar","/opt/app/app.jar"]
#ENTRYPOINT   ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:4445" "-Dspring.config.location=/opt/app/", "-jar","/opt/app/app.jar"]
EXPOSE 4444 4444
# debug port
EXPOSE 4445 4445